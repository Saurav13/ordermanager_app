import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Dimensions,
  ToastAndroid,
  AlertIOS,
  Platform,
  ActivityIndicator,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { FontAwesome5 } from "@expo/vector-icons";
import Firebase from "../../config/Firebase";
import { validateEmail } from "../utils/validations";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const ForgotPasswordForm = ({ navigation }) => {
  const [email, setEmail] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleLogin = () => {
    setLoading(true);
    if (!validateEmail(email)) {
      Platform.OS === "android"
        ? ToastAndroid.show("Invalid Email.", ToastAndroid.LONG)
        : AlertIOS.alert("Invalid Email.");
      setLoading(false);
      return false;
    }
    try {
      let errorMessage = "Oops! Somenthing went wrong. Please try again.";
      Firebase.auth()
        .sendPasswordResetEmail(email)
        .then((res) => {
          navigation.navigate("Login");
          Platform.OS === "android"
            ? ToastAndroid.show(
                "Password rest link has been sent to your email.",
                ToastAndroid.LONG
              )
            : AlertIOS.alert("Password rest link has been sent to your email.");
        })
        .catch((error) => {
          setLoading(false);
          Platform.OS === "android"
            ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
            : AlertIOS.alert(errorMessage);
        });
    } catch (err) {
      setLoading(false);
      Platform.OS === "android"
        ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
        : AlertIOS.alert(errorMessage);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.input}>
        <FontAwesome5 style={styles.inputIcon} name="user" size={18} />
        <TextInput
          placeholder="Email"
          value={email}
          onChangeText={(email) => setEmail(email)}
          placeholderTextColor="#000"
          style={styles.textInputField}
        />
      </View>

      <TouchableOpacity onPress={handleLogin} style={styles.buttonContainer}>
        {loading ? (
          <ActivityIndicator size={22} color="#fff" />
        ) : (
          <Text style={styles.buttonText}>SEND EMAIL</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  input: {
    flexDirection: "row",
    height: 40,
    width: width * 0.75,
    borderRadius: 10,
    backgroundColor: "#fff",
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  inputIcon: {
    paddingTop: 10,
    paddingRight: 15,
  },
  textInputField: {
    fontFamily: "Roboto-Regular",
    width: "100%",
  },
  buttonContainer: {
    borderColor: "#41D1D1",
    backgroundColor: "#41D1D1",
    borderWidth: 2,
    paddingVertical: 15,
  },
  buttonText: {
    textAlign: "center",
    color: "#fff",
    fontSize: 18,
    fontFamily: "Roboto-Bold",
  },
  forgotContainer: {
    paddingVertical: 6,
  },
  forgotText: {
    textAlign: "center",
    fontSize: 12,
    color: "#41D1D1",
    fontFamily: "Roboto-Regular",
  },
});

export default ForgotPasswordForm;
