import React, { useContext, useEffect } from "react";
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { FontAwesome5 } from "@expo/vector-icons";
import { UserContext } from "../navigation/UserProvider";

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;

const SideMenu = (props) => {
  const { user, venue, venueDetail, setVenue, baseURL } = useContext(
    UserContext
  );
  const handleBackButtonClick = () => {
    props.navigation.navigate("Orders");
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick);
  });

  return venue ? (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: "#fefefe",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <View
        style={{
          backgroundColor: "#33d0d0",
          height: "20%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            flexWrap: "wrap",
            marginTop: height * 0.04,
          }}
        >
          <View
            style={{
              width: "20%",
              alignItems: "center",
              paddingLeft: width * 0.05,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                setVenue(user, null, null, baseURL);
              }}
            >
              <FontAwesome5 name="user-circle" color="#fff" size={32} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: "80%",
              alignItems: "flex-start",
              paddingLeft: width * 0.02,
            }}
          >
            <Text
              style={{ fontFamily: "Roboto-Bold", fontSize: 24, color: "#fff" }}
            >
              {venueDetail.restaurant_name}
            </Text>
          </View>
        </View>
      </View>

      <View style={{ height: "70%" }}>
        <View style={styles.menuItems}>
          <Text style={styles.menuHeaderText}>ORDERS</Text>

          <TouchableOpacity onPress={() => props.navigation.navigate("Orders")}>
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={styles.menuItemIcon}
                  name="utensils"
                  size={18}
                />
              </View>
              <View style={styles.menuTextContainer}>
                <Text style={styles.menuItemText}>My Orders</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.menuItems}>
          <Text style={styles.menuHeaderText}>REPORTS</Text>

          <TouchableOpacity
            onPress={() => props.navigation.navigate("DailyReport")}
          >
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={styles.menuItemIcon}
                  name="chart-bar"
                  size={18}
                />
              </View>
              <View style={styles.menuTextContainer}>
                <Text style={styles.menuItemText}>Daily Report</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.menuItems}>
          <Text style={styles.menuHeaderText}>SETTINGS</Text>

          {/* <TouchableOpacity
                onPress={()=>props.navigation.navigate('Print')}
            >
                <View style={styles.menuItemContainer}>
                    <View style={styles.iconContainer}>
                        <FontAwesome5
                            style={styles.menuItemIcon}
                            name='print' size={18}
                        />
                    </View>
                    <View style={styles.menuTextContainer}>
                        <Text style={styles.menuItemText}>Thermal Printer</Text>
                    </View>
                </View>
            </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("Support");
            }}
          >
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={styles.menuItemIcon}
                  name="phone-square"
                  size={18}
                />
              </View>
              <View style={styles.menuTextContainer}>
                <Text style={styles.menuItemText}>Technical Support</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("Feedback");
            }}
          >
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={styles.menuItemIcon}
                  name="comment-alt"
                  size={17}
                />
              </View>
              <View style={{ ...styles.menuTextContainer, paddingLeft: 8 }}>
                <Text style={styles.menuItemText}>Feedback</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.menuItems}>
          <Text style={styles.menuHeaderText}>ACCOUNT & LEGAL</Text>

          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("Account");
            }}
          >
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={styles.menuItemIcon}
                  name="user-circle"
                  size={18}
                />
              </View>
              <View style={styles.menuTextContainer}>
                <Text style={styles.menuItemText}>My Account</Text>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("About");
            }}
          >
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={styles.menuItemIcon}
                  name="question-circle"
                  size={18}
                />
              </View>
              <View style={styles.menuTextContainer}>
                <Text style={styles.menuItemText}>About</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("Terms");
            }}
          >
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={{ ...styles.menuItemIcon, paddingLeft: 2 }}
                  name="file-alt"
                  size={18}
                />
              </View>
              <View style={{ ...styles.menuTextContainer, paddingLeft: 6 }}>
                <Text style={styles.menuItemText}>Terms & Conditions</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.menuItemContainer}>
              <View style={styles.iconContainer}>
                <FontAwesome5
                  style={{ ...styles.menuItemIcon, paddingLeft: 1 }}
                  name="sign-out-alt"
                  size={18}
                />
              </View>
              <View style={{ ...styles.menuTextContainer }}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate("Logout");
                  }}
                >
                  <Text style={styles.menuItemText}>Logout</Text>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>

      <View
        style={{
          paddingLeft: width * 0.03,
          height: "8%",
        }}
      >
        <Text style={{ fontSize: 9, paddingLeft: 3, marginBottom: -8 }}>
          Powered By
        </Text>
        <Image source={require("../../assets/images/power.png")} />
      </View>
    </ScrollView>
  ) : null;
};

const styles = StyleSheet.create({
  menuItems: {
    marginTop: 15,
    paddingLeft: width * 0.05,
  },
  menuItemContainer: {
    paddingTop: 8,
    flexDirection: "row",
  },
  iconContainer: {
    width: "7%",
    alignItems: "flex-start",
  },
  menuTextContainer: {
    width: "93%",
    alignItems: "flex-start",
    paddingLeft: 7,
  },
  menuItemText: {
    fontFamily: "Roboto-Regular",
    fontSize: 15,
  },
  menuHeaderText: {
    fontSize: 18,
    fontFamily: "Roboto-Bold",
    marginBottom: 2,
  },
});

export default SideMenu;
