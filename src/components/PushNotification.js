import { Notifications } from "expo";
import * as Permissions from "expo-permissions";
import { AlertIOS, Platform, ToastAndroid } from "react-native";
import { AsyncStorage } from "react-native";

export default async function registerForPushNotificationsAsync(
  idToken,
  baseURL
) {
  const PUSH_ENDPOINT = baseURL + "/user/expo/subscribe";
  const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  let finalStatus = status;
  if (status !== "granted") {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }
  if (finalStatus !== "granted") {
    const errorMessage = "Failed to get push token for push notification!";
    Platform.OS === "android"
      ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
      : AlertIOS.alert(errorMessage);
    return;
  }
  Platform.OS === "android"
    ? Notifications.createChannelAndroidAsync("default", {
        name: "default",
        sound: true,
        vibrate: true,
        priority: "max",
        vibrate: [0, 250, 250, 250],
      })
    : null;
  const token = await Notifications.getExpoPushTokenAsync();
  AsyncStorage.setItem("PUSH_TOKEN", token);
  fetch(PUSH_ENDPOINT, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + idToken,
    },
    body: JSON.stringify({
      expo_token: token,
    }),
  })
    .then((res) => res.json())
    .then((res) => {})
    .catch((err) => {
      alert("Coulnot register for push notification.");
    });
}
