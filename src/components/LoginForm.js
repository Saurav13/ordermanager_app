import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Dimensions,
  ToastAndroid,
  AlertIOS,
  Platform,
  ActivityIndicator,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { FontAwesome5 } from "@expo/vector-icons";
import Firebase from "../../config/Firebase";
import { validateEmail, validatePassword } from "../utils/validations";

const width = Dimensions.get("window").width;

const LoginForm = ({ navigation }) => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleLogin = () => {
    setLoading(true);
    if (!validateEmail(email)) {
      const errorMessage = "Invalid Email";
      Platform.OS === "android"
        ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
        : AlertIOS.alert(errorMessage);
      setLoading(false);
      return false;
    }
    if (!validatePassword(password)) {
      const errorMessage = "Enter Password";
      Platform.OS === "android"
        ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
        : AlertIOS.alert(errorMessage);
      setLoading(false);
      return false;
    }
    try {
      Firebase.auth()
        .signInWithEmailAndPassword(email, password)
        .catch((error) => {
          setLoading(false);
          switch (error.code) {
            case "auth/user-not-found":
              errorMessage = "Invalid Credentials.";
              break;
            case "auth/wrong-password":
              errorMessage = "Invalid Credentials.";
              break;
            default:
              errorMessage = "Could not process request. Pleas Try Again.";
          }
          Platform.OS === "android"
            ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
            : AlertIOS.alert(errorMessage);
        });
    } catch (err) {
      setLoading(false);
      const errorMessage = "Oops! Somenthing went wrong. Please try again.";
      Platform.OS === "android"
        ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
        : AlertIOS.alert(errorMessage);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.input}>
        <FontAwesome5 style={styles.inputIcon} name="user" size={18} />
        <TextInput
          placeholder="Email"
          value={email}
          onChangeText={(email) => setEmail(email)}
          placeholderTextColor="#000"
          style={styles.textInputField}
        />
      </View>
      <View style={styles.input}>
        <FontAwesome5 style={styles.inputIcon} name="unlock-alt" size={18} />
        <TextInput
          placeholder="Password"
          value={password}
          onChangeText={(password) => setPassword(password)}
          placeholderTextColor="#000"
          secureTextEntry
          style={styles.textInputField}
        />
      </View>

      <TouchableOpacity onPress={handleLogin} style={styles.buttonContainer}>
        {loading ? (
          <ActivityIndicator size={22} color="#fff" />
        ) : (
          <Text style={styles.buttonText}>SIGN IN</Text>
        )}
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("ForgotPassword");
        }}
        style={styles.forgotContainer}
      >
        <Text style={styles.forgotText}>Forgot Password ?</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: width / 8,
  },
  input: {
    flexDirection: "row",
    height: 40,
    width: width * 0.75,
    borderRadius: 10,
    backgroundColor: "#fff",
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  inputIcon: {
    paddingTop: 10,
    paddingRight: 15,
  },
  textInputField: {
    fontFamily: "Roboto-Regular",
    width: "100%",
  },
  buttonContainer: {
    marginTop: 8,
    borderColor: "#41D1D1",
    backgroundColor: "#41D1D1",
    borderWidth: 2,
    paddingVertical: 15,
  },
  buttonText: {
    textAlign: "center",
    color: "#fff",
    fontSize: 18,
    fontFamily: "Roboto-Bold",
  },
  forgotContainer: {
    paddingVertical: 9,
  },
  forgotText: {
    textAlign: "center",
    fontSize: 12,
    color: "#000",
    fontFamily: "Roboto-Regular",
  },
});

export default LoginForm;
