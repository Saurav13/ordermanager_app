import React, { useContext, useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { Card } from "react-native-elements";
import { FontAwesome5 } from "@expo/vector-icons";
import { UserContext } from "../navigation/UserProvider";

export default ({ item, currentURL }) => {
  const [loading, setLoading] = useState(false);
  const { user, setVenue } = useContext(UserContext);
  return (
    <TouchableOpacity
      onPress={() => {
        setLoading(true);
        setTimeout(() => {
          setVenue(user, item.id, item, currentURL);
          setLoading(false);
        }, 5000);
      }}
    >
      <Card>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ width: "90%" }}>
            <Text style={styles.text}>{item.restaurant_name}</Text>
          </View>
          <View style={{ width: "10%" }}>
            {loading ? (
              <ActivityIndicator size="small" color="#000" />
            ) : (
              <FontAwesome5 name="arrow-right" size={18} />
            )}
          </View>
        </View>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: "Roboto-Bold",
    fontSize: 14,
  },
});
