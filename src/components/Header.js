import React from "react";
import { Header } from "react-native-elements";

const HeaderMenu = ({ headerTitle, rightComponent, leftComponent }) => {
  return (
    <Header
      backgroundColor="#fff"
      leftComponent={leftComponent}
      centerComponent={{
        text: headerTitle,
        style: {
          color: "#000",
          fontSize: 18,
          fontFamily: "Roboto-Bold",
        },
      }}
      rightComponent={rightComponent}
    />
  );
};

export default HeaderMenu;
