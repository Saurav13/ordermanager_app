import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Image,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const aspectRatio = height / width;

const getTextColor = (status = null) => {
  if (status == "Rejected") {
    return "#d4322e";
  } else if (status == "Cancelled") {
    return "#d4322e";
  } else if (status == "AutoRejected") {
    return "#d4322e";
  } else if (status == "Accepted") {
    return "#20c281";
  } else if (status == "Completed") {
    return "#20c281";
  }
  return "#ffad37";
};

const formatTime = (time_in_sec) => {
  let min = Math.floor(time_in_sec / 60);
  let sec = time_in_sec - min * 60;
  min = min < 10 ? "0" + min : min;
  sec = sec < 10 ? "0" + sec : sec;
  return min + ":" + sec;
};

export default ({ detail, stopSound, navigation }) => {
  const [status, setStatus] = useState(detail.status);
  const [timer, setTimer] = useState(
    detail.timer < 180 && detail.status === "Pending"
      ? 180 - Number(detail.timer)
      : Number(detail.timer)
  );
  const [counterFlag, setCounterFlag] = useState("down");

  const getTime = () => {
    return timer;
  };

  useEffect(() => {
    // refactor function phase 2
    if (status === "Pending") {
      if (timer > 900) {
        setTimer(null);
        setStatus("Cancelled");
      }

      if (timer > 180) setCounterFlag("up");
      if (timer === 0) setCounterFlag("up");

      if (counterFlag === "down") {
        const countDown = setTimeout(() => {
          setTimer(timer - 1);
        }, 1000);
        return () => clearInterval(countDown);
      }

      if (counterFlag === "up") {
        const countUp = setTimeout(() => {
          setTimer(timer + 1);
        }, 1000);
        return () => clearInterval(countUp);
      }
    } else if (status === "Accepted") {
      console.log(timer);
      if (timer <= 0) {
        setTimer(null);
      } else {
        const countDown = setTimeout(() => {
          setTimer(timer - 1);
        }, 1000);
        return () => clearInterval(countDown);
      }
    } else if (status === "Rejected") {
      if (detail.isAutoRejected) {
        setStatus("AutoRejected");
      } else if (detail.isRefunded) {
        setStatus("Refunded");
      }
    } else {
      setTimer(null);
    }
  }, [timer, counterFlag, status]);

  return (
    <TouchableOpacity
      onPress={() => {
        stopSound();
        navigation.navigate("OrderDetails", {
          detail,
          status,
          timer,
          setStatus,
          setTimer,
          counterFlag,
        });
      }}
    >
      <View style={styles.container}>
        <View style={styles.statusContainer}>
          {detail.customer.address ? (
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ fontSize: 10, fontFamily: "Roboto-Bold" }}>
                Delivery
              </Text>
              <Image
                style={{ height: 45, width: 45 }}
                source={require("../../assets/images/Delivery.png")}
              />
            </View>
          ) : (
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ fontSize: 10, fontFamily: "Roboto-Bold" }}>
                Pickup
              </Text>
              <Image
                style={{ marginTop: 5, height: 42, width: 42 }}
                source={require("../../assets/images/Pickup.png")}
              />
            </View>
          )}
          {timer && status == "Pending" ? (
            <Text
              style={{
                fontFamily: "Roboto-Bold",
                color: counterFlag === "up" ? "#d4322e" : "#ffad37",
              }}
            >
              {formatTime(timer)}
            </Text>
          ) : null}
          {timer && status == "Accepted" ? (
            <Text style={{ fontFamily: "Roboto-Bold", color: "#20c281" }}>
              {formatTime(timer)}
            </Text>
          ) : null}
        </View>
        <View style={styles.borderRightContainer} />
        <View style={styles.detailContainer}>
          <View>
            <Text style={styles.contentName}>
              {detail.customer.first_name} {detail.customer.last_name}
            </Text>
          </View>
          {detail.customer.address ? (
            <View>
              <Text style={styles.contentAddress}>
                {detail.customer.postcode} - {detail.customer.address}
              </Text>
            </View>
          ) : null}

          <View>
            <Text
              style={{ ...styles.contentStatus, color: getTextColor(status) }}
            >
              {status === "AutoRejected" ? "Auto Rejected" : status}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    backgroundColor: "#fff",
    borderBottomColor: "#e2e2e2",
    borderBottomWidth: 1,
    height: aspectRatio < 1.6 ? hp("9%") : hp("13.5%"),
    // 10 % -> 12.5/13
  },
  statusContainer: {
    width: aspectRatio < 1.6 ? hp("8.5%") : hp("11.5%"),
    padding: hp("1%"),
    justifyContent: "center",
    alignItems: "center",
  },
  borderRightContainer: {
    width: hp("2%"),
    height: aspectRatio < 1.6 ? hp("7%") : hp("11%"),
    marginTop: hp("1%"),
    borderLeftWidth: 2,
    borderLeftColor: "#888",
  },
  detailContainer: {
    width: "70%",
    justifyContent: "center",
    alignItems: "flex-start",
    marginTop: hp("1%"),
    paddingLeft: wp("1%"),
    marginBottom: hp("1%"),
  },
  statusContainerText: {
    fontFamily: "Roboto-Bold",
  },
  contentName: {
    fontSize: 16,
    fontFamily: "Roboto-Regular",
    color: "#000",
  },
  contentAddress: {
    fontSize: 14,
    color: "#555",
    fontFamily: "Roboto-Regular",
  },
  contentStatus: {
    fontSize: 13,
    fontFamily: "Roboto-Regular",
  },
});
