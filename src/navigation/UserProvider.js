import React, { useState, useEffect, createContext } from "react";
import Firebase from "../../config/Firebase";
import { AsyncStorage } from "react-native";
import{
  API_KEY,
  AUTH_DOMAIN,
  DATABASE_URL,
  PROJECT_ID,
  MESSAGE_SENDER_ID,
  APP_ID,
  BASE_URL
} from 'react-native-dotenv';


export const UserContext = createContext({
  user: null,
  venue: null, // venue id
  venueDetail: null,
  baseURL: null,
  setVenue: () => {},
});


export default UserProvider = (props) => {
  const [url, setURL] = useState(null);
  const [user, setUser] = useState({
    user: null,
    venue: null, // venue id
    venueDetail: null,
    baseURL: url,
    setVenue: () => {},
  });

  const setVenue = (user, venueID, venueDetail, currentURL) => {
    setUser({
      user: user,
      venue: venueID,
      venueDetail: venueDetail,
      baseURL: currentURL,
      setVenue: setVenue,
    });
  };

  useEffect(() => {
    Firebase.auth().onAuthStateChanged((userAuth) => {
      AsyncStorage.getItem("baseURL").then((r) => {
        res=BASE_URL;
        setURL(res);
        if (userAuth) {
          setUser({
            baseURL: res,
            user: userAuth,
            venue: null,
            setVenue: setVenue,
            venueDetail: null,
          });
        } else {
          setUser({
            baseURL: res,
            user: null,
            venue: null,
            setVenue: setVenue,
            venueDetail: null,
          });
        }
      });
    });
  }, []);

  return (
    <UserContext.Provider value={user}>{props.children}</UserContext.Provider>
  );
};
