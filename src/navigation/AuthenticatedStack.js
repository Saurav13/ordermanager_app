import React, { useContext } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import SideMenu from "../components/SideMenu";
import Orders from "../screens/Orders";
import OrderDetails from "../screens/OrderDetails";
import Venues from "../screens/Venues";
import About from "../screens/About";
import Terms from "../screens/Terms";
import Feedback from "../screens/Feedback";
import Support from "../screens/Support";
import Account from "../screens/Account";
import DailyReport from "../screens/DailyReport";
import Accept from "../screens/Accept";
import Logout from "../screens/Logout";
import { UserContext } from "./UserProvider";

const Drawer = createDrawerNavigator();

export default AuthenticatedStack = () => {
  const { venue } = useContext(UserContext);
  return venue ? (
    <NavigationContainer>
      <Drawer.Navigator
        headerMode="none"
        drawerContent={(props) => <SideMenu {...props} />}
      >
        <Drawer.Screen name="Orders" component={Orders} />
        <Drawer.Screen name="OrderDetails" component={OrderDetails} />
        <Drawer.Screen name="Accept" component={Accept} />
        <Drawer.Screen name="Venues" component={Venues} />
        <Drawer.Screen name="Terms" component={Terms} />
        <Drawer.Screen name="Support" component={Support} />
        <Drawer.Screen name="Feedback" component={Feedback} />
        <Drawer.Screen name="About" component={About} />
        <Drawer.Screen name="Account" component={Account} />
        <Drawer.Screen name="DailyReport" component={DailyReport} />
        <Drawer.Screen name="Logout" component={Logout} />
      </Drawer.Navigator>
    </NavigationContainer>
  ) : (
    <Venues />
  );
};
