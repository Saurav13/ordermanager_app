import React, { useContext } from "react";
import { UserContext } from "./UserProvider";
import SignInStack from "./SignInStack";
import AuthenticatedStack from "./AuthenticatedStack";

export default function App() {
  const { user } = useContext(UserContext);
  return user ? <AuthenticatedStack /> : <SignInStack />;
}
