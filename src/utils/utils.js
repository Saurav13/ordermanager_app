import React from "react";
import { FontAwesome5 } from "@expo/vector-icons";
import {
  Clipboard,
  AlertIOS,
  ToastAndroid,
  Platform,
  Linking,
} from "react-native";

export const formatTime = (time_in_sec) => {
  if (time_in_sec) {
    let min = Math.floor(time_in_sec / 60);
    let sec = time_in_sec - min * 60;
    min = min < 10 ? "0" + min : min;
    sec = sec < 10 ? "0" + sec : sec;
    return min + ":" + sec;
  }
};

export const getTextColor = (status = null) => {
  if (
    status == "Rejected" ||
    status == "AutoRejected" ||
    status == "Refunded"
  ) {
    return "#d4322e";
  } else if (status == "Cancelled") {
    return "#d4322e";
  } else if (status == "Accepted") {
    return "#20c281";
  }
  if (status == "Completed") {
    return "#20c281";
  }
  return "#ffad37";
};

export const getIcon = (status = "Pending") => {
  let name = "exclamation-triangle";
  if (status === "Cancelled") {
    name = "times-circle";
  } else if (status == "Accepted") {
    name = "check-square";
  } else if (status == "Completed") {
    name = "check-double";
  } else if (
    status == "Refunded" ||
    status == "Rejected" ||
    status == "AutoRejected"
  ) {
    name = "times-circle";
  }
  return (
    <FontAwesome5
      style={{
        paddingRight: 7,
        color: getTextColor(status),
      }}
      name={name}
      size={15}
    />
  );
};

export const openMap = (lat, lng) => {
  const scheme = Platform.select({
    ios: "maps:0,0?q=",
    android: "geo:0,0?q=",
  });
  const latLng = `${lat},${lng}`;
  const label = "Delivery Address";
  const url = Platform.select({
    ios: `${scheme}${label}@${latLng}`,
    android: `${scheme}${latLng}(${label})`,
  });
  Linking.openURL(url);
};

export const copyToClipBoard = (detail) => {
  let dataString = ` 
  OrderID: ${detail.id} 
  FullName: ${detail.customer.first_name} ${detail.customer.last_name} 
  Email: ${detail.customer.email} 
  ChargeID: ${detail.charge_id} 
  Order Date: ${detail.order_placed_at}
  Phone Number: ${detail.customer.phone_no}
  Order Placed At: ${detail.order_placed_at}
  Status: ${detail.status}
  Payment Type: ${detail.payment_type}
  Order Type: ${detail.order_type}
  `;
  if (detail.order_type === "Delivery") {
    dataString += `
        Delivery Time: ${detail.delivery_time}
        Delivery Charge: ${detail.delivery_charge}
        `;
  }
  if (detail.order_items.length > 0) {
    detail.order_items.map((item) => {
      dataString += `
            Item Name: ${item.name}
            Item Price: ${item.price}
            Item Quantity: ${item.qty}
            Total Item Price : ${item.total_item_price}
        `;
      if (item.variant) {
        dataString += `
            Variant Name: ${item.variant.name}
            Variant Price: ${item.variant.price}
            Variant Quantity: ${item.variant.qty}
        `;
      }
      if (item.options.length) {
        item.options.length <= 0
          ? null
          : item.options.map((option) => {
              dataString += `
                    Option Name: ${option.group_name} - ${option.name}
                    Option Price: ${option.price}
                    Option Quantity: ${option.qty}
                `;
            });
      }
    });
  }
  dataString += `
    SubTotal: ${detail.subtotal}
    Discount: ${detail.discount}
    Tax: ${detail.tax}
    Grand Total: ${detail.grand_total}
  `;
  Clipboard.setString(dataString);
  Platform.OS === "android"
    ? ToastAndroid.show("Copied To Clipboard.", ToastAndroid.LONG)
    : AlertIOS.alert("Copied To Clipboard.");
};

export const buildURL = (
  baseURL,
  date = false,
  filter = false,
  paginationData = false
) => {
  let url = date ? baseURL + "?date=" + date : baseURL;
  if (filter) {
    url += date ? "&" : "?";
    url = url + `${filter.key}=${filter.value}`;
  }
  if (paginationData) {
    url += filter || date ? "&" : "?";
    url =
      url +
      `last_order_id=${paginationData.lastItem}&last_order_placed_at=${paginationData.lastOrderPlacedAt}`;
  }
  return url;
};
