import React, { useState, useContext, useEffect } from "react";
import { UserContext } from "../navigation/UserProvider";
import {
  View,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Platform,
  ToastAndroid,
  AlertIOS,
  BackHandler,
} from "react-native";
import HeaderMenu from "../components/Header";
import { Card } from "react-native-elements";
import { Icon } from "react-native-elements";
import AutoHeightWebView from "react-native-autoheight-webview";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const aspectRatio = height / width;

const Feedback = ({ navigation }) => {
  const [feedback, setFeedback] = useState(null);
  const [dataSending, setDataSending] = useState(false);

  const { user, venue, baseURL } = useContext(UserContext);

  const submitFeedback = () => {
    if (!feedback) {
      Platform.OS === "android"
        ? ToastAndroid.show("Please Enter Feedback.", ToastAndroid.LONG)
        : AlertIOS.alert("Please Enter Feedback.");
      return;
    }
    setDataSending(true);
    user.getIdToken().then(function (idToken) {
      const url = baseURL + "/feedback/submit";
      fetch(url, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + idToken,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          message: feedback,
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          Platform.OS === "android"
            ? ToastAndroid.show(res.message, ToastAndroid.LONG)
            : AlertIOS.alert(res.message);
          setFeedback("");
          setDataSending(false);
        })
        .catch((error) => {
          const errorMessage =
            "Oops! Something went wrong. Please try again later.";
          Platform.OS === "android"
            ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
            : AlertIOS.alert(errorMessage);
        });
    });
  };

  return (
    <ScrollView style={{ backgroundColor: "#e2e2e2" }}>
      <HeaderMenu
        navigation={navigation}
        headerTitle="FEEDBACK"
        rightComponent={<View></View>}
        leftComponent={
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <View style={{ paddingLeft: 15 }}>
              <Icon name="bars" size={26} color="#000" type="font-awesome" />
            </View>
          </TouchableOpacity>
        }
      />
      <Card>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Text
            style={{
              fontSize: 14,
              fontFamily: "Roboto-Bold",
              alignSelf: "center",
            }}
          >
            Send your valuable feedback or questions
          </Text>
          <AutoHeightWebView
            style={{
              marginTop: 10,
              width: aspectRatio < 1.6 ? wp("88%") : wp("83%"),
            }}
            customScript={``}
            customStyle={`
                        p{
                            font-size: 14px !important;
                            color: '#333';
                            text-align: justify;
                        }
                    `}
            files={[
              {
                href: "cssfileaddress",
                type: "text/css",
                rel: "stylesheet",
              },
            ]}
            source={{
              html: `<html><head></head><body><p>If you have any questions or suggestions on how we can improve the order
                    delivery experience for your business or your respective customers please
                    include it here.</p></body></html>`,
            }}
            scalesPageToFit={true}
            viewportContent={"width=device-width, user-scalable=no"}
          />
          <TextInput
            multiline={true}
            value={feedback}
            placeholder={"Write your feedback here..."}
            placeholderTextColor="#888"
            numberOfLines={120}
            onChangeText={(value) => setFeedback(value)}
            textAlignVertical="top"
            style={{
              borderColor: "#4DD1D1",
              borderWidth: 3,
              padding: 10,
              height: hp("20%"),
              marginTop: 10,
              width: wp("84%"),
              borderRadius: 20,
              alignSelf: "center",
            }}
          />
          <TouchableOpacity
            onPress={submitFeedback}
            style={{
              backgroundColor: "#4DD1D1",
              marginTop: 20,
              borderColor: "#4DD1D1",
              width: width * 0.8,
              marginLeft: 5,
              height: hp("7%"),
              alignItems: "center",
              alignSelf: "center",
              justifyContent: "center",
            }}
          >
            {dataSending ? (
              <ActivityIndicator size="large" color="#fff" />
            ) : (
              <Text
                style={{
                  color: "#fff",
                  fontSize: 18,
                  fontFamily: "Roboto-Bold",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                Send Feedback
              </Text>
            )}
          </TouchableOpacity>
          <View style={{ alignItems: "center", paddingTop: 10 }}>
            <Text
              style={{
                fontSize: 12,
                fontFamily: "Roboto-Regular",
                color: "#888",
              }}
            >
              Once submitted, One Corner team
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontFamily: "Roboto-Regular",
                color: "#888",
              }}
            >
              will review it and reach out to you.
            </Text>
          </View>
        </View>
      </Card>
    </ScrollView>
  );
};

export default Feedback;
