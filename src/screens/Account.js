import React, { useContext, useEffect } from "react";
import { UserContext } from "../navigation/UserProvider";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
  BackHandler,
} from "react-native";
import HeaderMenu from "../components/Header";
import { FontAwesome5 } from "@expo/vector-icons";
import MapView from "react-native-maps";
import { Icon, Card } from "react-native-elements";
import AutoHeightWebView from "react-native-autoheight-webview";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const aspectRatio = height / width;

const Account = ({ navigation }) => {
  const { venueDetail } = useContext(UserContext);

  return (
    <ScrollView style={{ backgroundColor: "#e2e2e2" }}>
      <HeaderMenu
        navigation={navigation}
        headerTitle="MY ACCOUNT"
        rightComponent={<View></View>}
        leftComponent={
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <View style={{ paddingLeft: 15 }}>
              <Icon name="bars" size={26} color="#000" type="font-awesome" />
            </View>
          </TouchableOpacity>
        }
      />
      <Card>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {venueDetail.url_icon ? (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                width: aspectRatio < 1.6 ? wp("90%") : wp("83%"),
              }}
            >
              <Image
                style={{
                  height: hp("30%"),
                  width: aspectRatio < 1.6 ? wp("90%") : wp("83%"),
                }}
                source={{ uri: venueDetail.url_icon }}
              />
            </View>
          ) : null}
          <View
            style={{
              width: width * 0.85,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20,
            }}
          >
            <Text
              style={{
                fontSize: 18,
                fontFamily: "Roboto-Bold",
                marginBottom: 10,
              }}
            >
              {venueDetail.restaurant_name}
            </Text>
          </View>

          <View style={styles.details}>
            <View style={styles.detailIcon}>
              <FontAwesome5 name="envelope" size={16} />
            </View>
            <View style={styles.detailContent}>
              <Text>{venueDetail.email}</Text>
            </View>
          </View>

          <View style={styles.details}>
            <View style={styles.detailIcon}>
              <FontAwesome5 name="utensils" size={16} />
            </View>
            <View style={styles.detailContent}>
              <Text>{venueDetail.cuisine}</Text>
            </View>
          </View>

          <View style={styles.details}>
            <View style={styles.detailIcon}>
              <FontAwesome5 name="map-marker-alt" size={16} />
            </View>
            <View style={styles.detailContent}>
              <Text>
                {venueDetail.street_number} {venueDetail.street_name}{" "}
                {venueDetail.suburb},{venueDetail.state} {venueDetail.country}
              </Text>
            </View>
          </View>

          <View style={styles.details}>
            <View style={styles.detailIcon}>
              <FontAwesome5 name="phone" size={16} />
            </View>
            <View style={styles.detailContent}>
              <Text>{venueDetail.phone_number}</Text>
            </View>
          </View>
          {venueDetail.about_restaurant ? (
            <View style={styles.details}>
              <View style={styles.detailIcon}>
                <FontAwesome5 name="comment-alt" size={16} />
              </View>
              <View style={styles.detailContent}>
                <AutoHeightWebView
                  style={{ width: aspectRatio < 1.6 ? wp("70%") : wp("69%") }}
                  customScript={``}
                  customStyle={`
                                        p{
                                            font-size: 14px !important;
                                            text-align: justify;
                                        }
                                    `}
                  files={[
                    {
                      href: "cssfileaddress",
                      type: "text/css",
                      rel: "stylesheet",
                    },
                  ]}
                  source={{
                    html: `<html><head></head><body><p>${venueDetail.about_restaurant}</p></body></html>`,
                  }}
                />
              </View>
            </View>
          ) : null}

          <View style={styles.mapContainer}>
            <MapView
              style={styles.mapView}
              initialRegion={{
                latitude: Number(venueDetail.location.latitude),
                longitude: Number(venueDetail.location.longitude),
                latitudeDelta: 0.004,
                longitudeDelta:
                  (Dimensions.get("screen").width /
                    Dimensions.get("screen").height) *
                  0.004,
              }}
            >
              <MapView.Marker
                coordinate={{
                  latitude: Number(venueDetail.location.latitude),
                  longitude: Number(venueDetail.location.longitude),
                }}
              />
            </MapView>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              padding: 5,
              marginTop: 15,
            }}
          >
            <Text style={{ fontSize: 12, fontFamily: "Roboto-Regular" }}>
              * Your account details can only be modified from the web
              interface.
            </Text>
          </View>
        </View>
      </Card>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  details: {
    flex: 1,
    flexDirection: "row",
    paddingLeft: 20,
    paddingTop: 10,
  },
  mapContainer: {
    backgroundColor: "#fff",
    flex: 1,
    marginTop: 20,
    marginBottom: hp("5%"),
  },
  detailIcon: {
    width: "10%",
    alignItems: "flex-start",
  },
  detailContent: {
    width: "90%",
  },
  mapView: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    height: hp("40%"),
    width: aspectRatio < 1.6 ? wp("90%") : wp("83%"),
  },
});

export default Account;
