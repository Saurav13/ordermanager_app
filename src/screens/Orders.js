import { Notifications } from "expo";
import { Audio } from "expo-av";
import { ActionSheet, Root } from "native-base";
import React, { useContext, useEffect, useState } from "react";
import {
  ActivityIndicator,
  AlertIOS,
  AppState,
  BackHandler,
  Dimensions,
  Platform,
  RefreshControl,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { Icon } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import DateTimePickerModal from "react-native-modal-datetime-picker";

import HeaderMenu from "../components/Header";
import OrderItem from "../components/OrderItem";
import { UserContext } from "../navigation/UserProvider";
import { buildURL } from "../utils/utils";

const options = ["Pending", "Accepted", "Fulfilled", "Rejected"];

var BUTTONS = [
  { text: "Pending", icon: "alert", iconColor: "#ffad37" },
  { text: "Accepted", icon: "checkbox", iconColor: "#20c281" },
  { text: "Rejected", icon: "trash", iconColor: "#fa213b" },
  { text: "Auto Rejected", icon: "archive", iconColor: "#fa213b" },
  { text: "Completed", icon: "checkmark", iconColor: "#25de5b" },
  { text: "Cancelled", icon: "close-circle", iconColor: "#fa213b" },
  { text: "Refunded", icon: "cash", iconColor: "#ea943b" },
  { text: "Delivery", icon: "bicycle", iconColor: "#25de5b" },
  { text: "Pickup", icon: "walk", iconColor: "#ea943b" },
];

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;
const soundObject = new Audio.Sound();
const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 0;
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  );
};

export default Orders = ({ navigation }) => {
  // tdltr; redux implementaions
  const [orderItems, setOrderItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [noOrders, setNoOrders] = useState(false);
  const [activityIndicator, setActivityIndicator] = useState(false);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [currentFilter, setCurrentFilter] = useState(null);
  const [currentDateFilter, setCurrentDateFilter] = useState(null);
  const [sound, setSound] = useState(false);

  const { user, venue, baseURL } = useContext(UserContext);

  const handleAppStateChange = (nextAppState) => {
    if (nextAppState === "active") {
      reloadPage();
    }
  };

  const formatHeaderData = (date) => {
    let format_date = new Date(date).toDateString();
    let res = format_date.split(" ");
    format_date = res[0] + ", " + res[2] + " " + res[1] + " " + res[3];
    return format_date;
  };

  const getDate = (_) => {
    let today = new Date();
    return formatHeaderData(today);
  };

  const playSound = (_) => {
    soundObject.getStatusAsync().then((res) => {
      if (res.isLoaded) {
        setSound(true);
        soundObject.playAsync();
      }
    });
  };

  const loadSound = (_) => {
    setSound(false);
    soundObject.getStatusAsync().then((res) => {
      if (!res.isLoaded) {
        soundObject
          .loadAsync(require("../../assets/sound/clock.mp3"))
          .catch((err) => {
            const errorMessage = "Coulnot load order notification sound.";
            Platform.OS === "android"
              ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
              : AlertIOS.alert(errorMessage);
          });
      }
    });
  };

  const stopSound = (_) => {
    soundObject.getStatusAsync().then((res) => {
      if (res.isLoaded) {
        setSound(false);
        soundObject.stopAsync();
      }
    });
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleNotification = (notification) => {
    sound ? null : playSound();
    if (notification.origin == "received") reloadPage();
    if (notification.origin == "selected") {
      stopSound();
      reloadPage();
    }
  };

  const handleConfirm = (date) => {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    let complete_date = year + "-" + month + "-" + day;
    hideDatePicker();
    setCurrentDateFilter(complete_date);
    const url = buildURL(
      baseURL + `/venues/${venue}/orders/list`,
      complete_date,
      currentFilter
    );
    fetchData(url);
  };

  const filterByStatus = (filterStatus) => {
    let key =
      filterStatus == "Delivery" || filterStatus == "Pickup"
        ? "order_type"
        : "status";
    setCurrentFilter({ key: key, value: filterStatus });
    const url = buildURL(
      baseURL + `/venues/${venue}/orders/list`,
      currentDateFilter,
      {
        key: key,
        value: filterStatus,
      }
    );
    fetchData(url);
  };

  const fetchData = (url) => {
    setLoading(true);
    user.getIdToken().then(function (idToken) {
      fetch(url, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + idToken,
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      })
        .then((res) => res.json())
        .then((res) => {
          // console.log(url,res);
          if (res.items.length == 0) {
            setNoOrders(true);
          } else {
            setOrderItems([...res.items]);
            setNoOrders(false);
          }
          setLoading(false);
          setActivityIndicator(false);
        })
        .catch((error) => {
          const errorMessage = "Oops! Something went wrong. Please try again.";
          Platform.OS === "android"
            ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
            : AlertIOS.alert(errorMessage);
        });
    });
  };

  const reloadPage = (_) => {
    setLoading(true);
    setCurrentDateFilter(false);
    setCurrentFilter(false);
    const url = `${baseURL}/venues/${venue}/orders/list`;
    fetchData(url);
  };

  const loadMoreOrders = (_) => {
    if (orderItems.length > 6) {
      let orderItem = orderItems[orderItems.length - 1];
      let lastItemId = orderItem.id;
      let lastOrderPlacedAt = orderItem.order_placed_at;
      let paginationData = {
        lastItem: lastItemId,
        lastOrderPlacedAt: lastOrderPlacedAt,
      };
      let url = buildURL(
        `${baseURL}/venues/${venue}/orders/list`,
        currentDateFilter,
        currentFilter,
        paginationData
      );
      user.getIdToken().then(function (idToken) {
        fetch(url, {
          method: "GET",
          headers: {
            Authorization: "Bearer " + idToken,
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        })
          .then((res) => res.json())
          .then((res) => {
            if (res.items.length == 0) {
              setActivityIndicator(false);
            } else {
              setOrderItems([...orderItems, ...res.items]);
              setActivityIndicator(false);
            }
          })
          .catch((error) => {
            const errorMessage =
              "Oops! Something went wrong. Please try again later.";
            Platform.OS === "android"
              ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
              : AlertIOS.alert(errorMessage);
            setActivityIndicator(false);
          });
      });
    }
  };

  useEffect(() => {
    loadSound();
    stopSound();
    setCurrentDateFilter(false);
    setCurrentFilter(false);
    let url = `${baseURL}/venues/${venue}/orders/list`;
    fetchData(url);
    Notifications.addListener(handleNotification);
    AppState.addEventListener("change", handleAppStateChange);
  }, []);

  return (
    <Root>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={false}
            onRefresh={() => {
              setCurrentDateFilter(null);
              setCurrentFilter(null);
              reloadPage();
            }}
          />
        }
        style={{ backgroundColor: "#e2e2e2" }}
        stickyHeaderIndices={[0]}
        showsVerticalScrollIndicator={false}
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            setActivityIndicator(true);
            loadMoreOrders();
          }
        }}
        scrollEventThrottle={800}
      >
        <HeaderMenu
          navigation={navigation}
          headerTitle={
            currentDateFilter ? (
              <View>
                <Text
                  style={{
                    fontFamily: "Roboto-Bold",
                    fontSize: 18,
                  }}
                >
                  MY ORDERS
                </Text>
                <Text
                  style={{
                    fontFamily: "Roboto-Bold",
                    fontSize: 13,
                  }}
                >
                  {formatHeaderData(currentDateFilter)}
                </Text>
              </View>
            ) : (
              <View>
                <Text
                  style={{
                    fontFamily: "Roboto-Bold",
                    fontSize: 18,
                  }}
                >
                  MY ORDERS
                </Text>
                <Text
                  style={{
                    fontFamily: "Roboto-Bold",
                    fontSize: 13,
                  }}
                >
                  {getDate()}
                </Text>
              </View>
            )
          }
          leftComponent={
            <TouchableOpacity
              onPress={() => {
                navigation.openDrawer();
              }}
            >
              <View style={{ paddingLeft: 15 }}>
                <Icon name="bars" size={26} color="#000" type="font-awesome" />
              </View>
            </TouchableOpacity>
          }
          rightComponent={
            <View
              style={{
                flexDirection: "row",
              }}
            >
              <TouchableOpacity
                style={{
                  width: width * 0.1,
                  paddingRight: 5,
                  height: height * 0.08,
                  justifyContent: "center",
                }}
              >
                <Icon
                  onPress={showDatePicker}
                  name="calendar"
                  size={24}
                  color="#000"
                  type="font-awesome"
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: width * 0.09,
                  height: height * 0.08,
                  justifyContent: "center",
                }}
              >
                <Icon
                  onPress={() =>
                    ActionSheet.show(
                      {
                        options: BUTTONS,
                        title: (
                          <Text
                            style={{
                              alignItems: "center",
                              fontFamily: "Roboto-Bold",
                            }}
                          >
                            Filter By Status
                          </Text>
                        ),
                      },
                      (args) => {
                        let filterStatus = null;
                        switch (args) {
                          case 0:
                            filterStatus = "Pending";
                            break;
                          case 1:
                            filterStatus = "Accepted";
                            break;
                          case 2:
                            filterStatus = "Rejected";
                            break;
                          case 3:
                            filterStatus = "AutoRejected";
                            break;
                          case 4:
                            filterStatus = "Completed";
                            break;
                          case 5:
                            filterStatus = "Cancelled";
                            break;
                          case 6:
                            filterStatus = "Refunded";
                            break;
                          case 7:
                            filterStatus = "Delivery";
                            break;
                          case 8:
                            filterStatus = "Pickup";
                            break;
                          default:
                            filterStatus = null;
                        }
                        filterStatus ? filterByStatus(filterStatus) : null;
                      }
                    )
                  }
                  name="filter"
                  size={24}
                  color="#000"
                  type="font-awesome"
                />
              </TouchableOpacity>
            </View>
          }
        />
        <DateTimePickerModal
          isVisible={isDatePickerVisible}
          style={{ fontSize: 10 }}
          mode="date"
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
          is24Hour={false}
        />
        <View
          style={{
            borderBottomWidth: 3,
            borderBottomColor: "#e2e2e2",
          }}
        />
        {loading ? (
          <View style={styles.logoContainer}>
            <ActivityIndicator size={70} color="#4DD1D1" />
          </View>
        ) : noOrders ? (
          <View style={styles.logoContainer}>
            <Text style={{ fontFamily: "Roboto-Bold", color: "#888" }}>
              We could not find any orders.
            </Text>
          </View>
        ) : (
          <View>
            {orderItems.map((item, index) => (
              <OrderItem
                stopSound={stopSound}
                navigation={navigation}
                detail={item}
                key={index}
              />
            ))}
            {activityIndicator ? (
              <View
                style={{
                  backgroundColor: "#fff",
                }}
              >
                <ActivityIndicator size={30} color="#4DD1D1" />
              </View>
            ) : null}
          </View>
        )}
      </ScrollView>
    </Root>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    marginTop: height / 7,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
  logoText: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#fff",
  },
});
