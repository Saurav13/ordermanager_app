import React, { useEffect, useContext } from "react";
import {
  AsyncStorage,
  View,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import Firebase from "../../config/Firebase";
import { UserContext } from "../navigation/UserProvider";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

export default Logout = ({ navigation }) => {
  const { user, baseURL } = useContext(UserContext);
  const PUSH_ENDPOINT = baseURL + "/user/expo/unsubscribe";
  const logout = (_) => {
    Firebase.auth().signOut();
    
  };
  useEffect(() => {
    AsyncStorage.getItem("PUSH_TOKEN")
      .then((res) => {
        if (res) {
          user.getIdToken().then((idToken) => {
            fetch(PUSH_ENDPOINT, {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + idToken,
              },
              body: JSON.stringify({
                expo_token: res,
              }),
            })
              .then((res) => res.json())
              .then((res) => {
                logout();
              })
              .catch((err) => {
                logout();
              });
          });
        } else {
          logout();
        }
      })
      .catch((err) => {
        logout();
      });
  }, []);

  return (
    <View style={{ flex: 1, backgroundColor: "#e2e2e2" }}>
      <View
        style={{
          marginTop: height / 3,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <ActivityIndicator size={70} color="#41D1D1" />
      </View>
    </View>
  );
};
