import React, { useState, useEffect, useContext } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  KeyboardAvoidingView,
  Dimensions,
  ActivityIndicator,
  Platform,
  AlertIOS,
  ToastAndroid,
} from "react-native";
import { BASE_URL } from "react-native-dotenv";
import LoginForm from "../components/LoginForm";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AsyncStorage } from "react-native";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

export default Login = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [changeURL, setChangeURL] = useState(false);
  const [pressCount, setPressCount] = useState(null);

  const handleCount = (_) => {
    pressCount > 30 ? setChangeURL(true) : setPressCount(pressCount + 1);
  };

 

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 7000);
  }, []);

  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.loadingLogoContainer}>
          <ActivityIndicator size={70} color="#41D1D1" />
        </View>
      ) : (
        <KeyboardAvoidingView behavior="position">
          <View style={styles.logoContainer}>
            <TouchableOpacity
              onPress={() => {
                handleCount();
              }}
            >
              <Image
                style={styles.logo}
                source={require("../../assets/images/loginLogo.png")}
              />
            </TouchableOpacity>
            <Text style={styles.logoText}>TAKE ORDERS FOR</Text>
            <Text style={styles.logoText}>YOUR BUSINESS</Text>
          </View>
          <View style={styles.formContainer}>
            <LoginForm navigation={navigation} />
          </View>
          {changeURL ? (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <TouchableOpacity
                  style={{
                    backgroundColor: "#4DD1D1",
                    height: 45,
                    alignItems: "center",
                    justifyContent: "center",
                    margin: 5,
                    width: width * 0.35,
                  }}
                  onPress={() => {
                    setNewURL("https://staging.1crnr.com/api/app", "Staging");
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 16,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    STAGING
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor: "#ffad37",
                    height: 45,
                    alignItems: "center",
                    justifyContent: "center",
                    margin: 5,
                    width: width * 0.35,
                  }}
                  onPress={() => {
                    setNewURL(
                      "https://development.1crnr.com/api/app",
                      "Development"
                    );
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 16,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    DEVELOPMENT
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "row",
                }}
              >
                <TouchableOpacity
                  style={{
                    backgroundColor: "#20c281",
                    height: 45,
                    alignItems: "center",
                    justifyContent: "center",
                    margin: 5,
                    width: width * 0.35,
                  }}
                  onPress={() => {
                    setNewURL("https://onecorner.com.au/api/app", "Production");
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 16,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    PRODUCTION
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor: "#d4322e",
                    height: 45,
                    alignItems: "center",
                    justifyContent: "center",
                    margin: 5,
                    width: width * 0.35,
                  }}
                  onPress={() => {
                    setChangeURL(false);
                    setPressCount(null);
                  }}
                >
                  <Text
                    style={{
                      color: "#fff",
                      fontSize: 16,
                      fontFamily: "Roboto-Bold",
                    }}
                  >
                    CANCEL
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </KeyboardAvoidingView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e2e2e2",
  },
  loadingLogoContainer: {
    marginTop: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  loadingLogo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
  logoContainer: {
    marginTop: height / 5,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    marginBottom: 10,
  },
  logoText: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#333",
  },
  formContainer: {
    alignItems: "center",
    marginTop: 15,
  },
});

