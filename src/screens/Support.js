import {FontAwesome5} from '@expo/vector-icons';
import React, {useContext, useEffect, useState} from 'react';
import {ActivityIndicator, AlertIOS, BackHandler, Dimensions, Linking, Platform, ScrollView, StyleSheet, Text, TextInput, ToastAndroid, TouchableOpacity, View,} from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import {BASE_URL} from 'react-native-dotenv';
import {Card} from 'react-native-elements';
import {Icon} from 'react-native-elements';
import {heightPercentageToDP as hp, widthPercentageToDP as wp,} from 'react-native-responsive-screen';
import {WebView} from 'react-native-webview';

import HeaderMenu from '../components/Header';
import {UserContext} from '../navigation/UserProvider';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const aspectRatio = height / width;

const Support = ({navigation}) => {
  const [supportRequest, setSupportRequest] = useState('');
  const [dataSending, setDataSending] = useState(false);
  const { user, baseURL } = useContext(UserContext);

  const submitSupportRequest = () => {
    if (!supportRequest) {
      Platform.OS === 'android'
        ? ToastAndroid.show('Please Enter Your Request.', ToastAndroid.LONG)
        : AlertIOS.alert('Please Enter Your Request.');
      return;
    }
    setDataSending(true);
    user.getIdToken().then(function (idToken) {
      const url = baseURL + '/support/submit';
      fetch(url, {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + idToken,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          message: supportRequest,
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          Platform.OS === 'android'
            ? ToastAndroid.show(
                'Support Request Received Successfully.',
                ToastAndroid.LONG
              )
            : AlertIOS.alert('Support Request Received Successfully.');
          setSupportRequest('');
          setDataSending(false);
        })
        .catch((err) => {
          const errorMessage =
            'Oops! Something went wrong. Please try again later.';
          Platform.OS === 'android'
            ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
            : AlertIOS.alert(errorMessage);
        });
    });
  };

  return (
    <ScrollView style={{ backgroundColor: '#e2e2e2' }}>
      <HeaderMenu
        navigation={navigation}
        headerTitle='SUPPORT'
        rightComponent={<View></View>}
        leftComponent={
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <View style={{ paddingLeft: 15 }}>
              <Icon name="bars" size={26} color="#000" type="font-awesome" />
            </View>
          </TouchableOpacity>
        }
      />
      <Card>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <View style={{ alignItems: "center" }}>
            <Text
              style={{
                fontSize: 15,
                fontFamily: "Roboto-Bold",
                alignSelf: "center",
              }}
            >
              Non-urgent questions or corners
            </Text>
          </View>
          <AutoHeightWebView
            style={{
              marginTop: 10,
              width: aspectRatio < 1.6 ? wp("88%") : wp("81%"),
            }}
            customScript={``}
            customStyle={`
                        p{
                            font-size: 14px !important;
                            text-align: justify;
                        }
                    `}
            files={[
              {
                href: "cssfileaddress",
                type: "text/css",
                rel: "stylesheet",
              },
            ]}
            source={{
              html: `<html><head></head><body><p>Please include any questions or concerns you have regarding the online order system here or alternatively you can send us an email at support@onecorner.com.au</p></body></html>`,
            }}
            scalesPageToFit={true}
            viewportContent={"width=device-width, user-scalable=no"}
          />
          <TextInput
            multiline={true}
            value={supportRequest}
            onChangeText={(value) => setSupportRequest(value)}
            numberOfLines={120}
            placeholder={'Write your questions or concerns here...'}
            placeholderTextColor='#888'
            textAlignVertical='top'
            style={{
              borderColor: '#4DD1D1',
              borderWidth: 3,
              padding: 10,
              height: hp('15%'),
              width: wp('84%'),
              borderRadius: 20,
              marginTop: 12,
              alignSelf: 'center',
            }}
          />
          <TouchableOpacity
            onPress={submitSupportRequest}
            style={{
              backgroundColor: "#4DD1D1",
              marginTop: hp("2%"),
              borderColor: "#4DD1D1",
              width: wp("80%"),
              marginLeft: 5,
              height: hp("6%"),
              alignSelf: "center",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {dataSending ? (
              <ActivityIndicator size="large" color="#fff" />
            ) : (
              <Text
                style={{
                  color: '#fff',
                  justifyContent: 'center',
                  alignItems: 'center',
                  fontFamily: 'Roboto-Bold',
                  fontSize: 15,
                }}
              >
                Submit Support Request
              </Text>
            )}
          </TouchableOpacity>
          <View style={{ alignItems: 'center' }}>
            <Text
              style={{
                fontSize: 11,
                fontFamily: 'Roboto-Regular',
                color: '#888',
              }}
            >
              Once submitted, One Corner team will respond
            </Text>
            <Text
              style={{
                fontSize: 11,
                fontFamily: "Roboto-Regular",
                color: "#888",
              }}
            >
              to your questions shortly.
            </Text>
          </View>
          <View
            style={{
              borderBottomWidth: 3,
              borderBottomColor: "#888",
              marginTop: 20,
              marginBottom: 20,
            }}
          />
          <View style={{ alignItems: 'center' }}>
            <Text style={{ fontSize: 15, fontFamily: 'Roboto-Bold' }}>
              Only Urgent Requests
            </Text>
          </View>
          <AutoHeightWebView
            style={{
              marginTop: 10,
              width: aspectRatio < 1.6 ? wp('88%') : wp('81%'),
            }}
            customScript={``}
            customStyle={`
                        p{
                            font-size: 14px !important;
                            text-align: justify;
                        }
                    `}
            files={[
              {
                href: 'cssfileaddress',
                type: 'text/css',
                rel: 'stylesheet',
              },
            ]}
            source={{
              html: `<html><head></head><body><p>This support line is only available for urgent requests, like if your order online
                    system has stopped working or you have an issue with your order in the real-time.
                    All other queries should only come through as Non-urgent requests.</p></body></html>`,
            }}
            scalesPageToFit={true}
            viewportContent={'width=device-width, user-scalable=no'}
          />
          <Text
            style={{
              fontSize: 13,
              fontFamily: "Roboto-Regular",
              color: "#333",
              marginTop: 0,
            }}
          ></Text>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${'+61480032427'}`);
            }}
            style={{
              backgroundColor: '#4DD1D1',
              borderColor: '#4DD1D1',
              width: wp('80%'),
              height: hp('6%'),
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
              alignSelf: 'center',
            }}
          >
            <FontAwesome5
              style={{ paddingRight: 10 }}
              name='phone-volume'
              size={15}
              color='#fff'
            />
            <Text
              style={{
                color: '#fff',
                justifyContent: 'center',
                alignItems: 'center',
                fontFamily: 'Roboto-Bold',
                fontSize: 18,
              }}
            >
              Call One Corner
            </Text>
          </TouchableOpacity>
        </View>
      </Card>
    </ScrollView>
  );
};

export default Support;
