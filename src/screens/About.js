import React, { useState, useEffect, useContext } from "react";
import { UserContext } from "../navigation/UserProvider";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
  ActivityIndicator,
  BackHandler,
  Platform,
  AlertIOS,
  ToastAndroid,
} from "react-native";
import HeaderMenu from "../components/Header";
import { Card } from "react-native-elements";
import { Icon } from "react-native-elements";
import AutoHeightWebView from "react-native-autoheight-webview";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const aspectRatio = height / width;

const About = ({ navigation }) => {
  const [about, setAbout] = useState("");
  const [loading, setLoading] = useState(true);
  const { user, venueDetail, baseURL, setBaseURL } = useContext(UserContext);

  const getAboutInfo = async (url, idToken) => {
    await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + idToken,
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => {
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
      })
      .then(([status,res]) => {
        console.log(status);
        if(status==200){
        setAbout(res.content);
        setLoading(false);
      }
        
      })
      .catch((error) => {
        const errorMessage =
          "Oops! Something went wrong. Please try again later.";
        Platform.OS === "android"
          ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
          : AlertIOS.alert(errorMessage);
      });
  };

  useEffect(() => {
    user.getIdToken(true).then(function (idToken) {
      let url = `${baseURL}/about`;
      getAboutInfo(url, idToken);
    });
  }, []);

  return (
    <ScrollView style={{ backgroundColor: "#e2e2e2" }}>
      <HeaderMenu
        navigation={navigation}
        headerTitle="ABOUT"
        rightComponent={<View></View>}
        leftComponent={
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <View style={{ paddingLeft: 15 }}>
              <Icon name="bars" size={26} color="#000" type="font-awesome" />
            </View>
          </TouchableOpacity>
        }
      />
      {loading ? (
        <View style={styles.logoContainer}>
          <ActivityIndicator size={70} color="#4DD1D1" />
        </View>
      ) : (
        <Card>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <View>
              <Image
                style={{ marginTop: 10 }}
                source={require("../../assets/images/logo-black.png")}
              />

              <Text
                style={{ fontSize: 13, color: "#888", alignSelf: "center" }}
              >
                System Version 0.0.0-beta
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 25,
              }}
            >
              {about ? (
                <AutoHeightWebView
                  style={{
                    alignSelf: "center",
                    width: aspectRatio < 1.6 ? wp("88%") : wp("82%"),
                  }}
                  customScript={``}
                  customStyle={`
                                        body{
                                            font-size: 14px !important;
                                            text-align: justify;
                                        }
                                    `}
                  files={[
                    {
                      href: "cssfileaddress",
                      type: "text/css",
                      rel: "stylesheet",
                    },
                  ]}
                  source={{
                    html: `<html><head></head><body>${about}</body></html>`,
                  }}
                  viewportContent={"width=device-width, user-scalable=no"}
                />
              ) : null}
            </View>
            <View
              style={{
                flex: 1,
                borderTopWidth: 2,
                marginTop: 20,
                borderTopColor: "#888",
              }}
            >
              <Text style={{ marginTop: 10, marginBottom: 20 , fontSize:10}}>
                We would love to hear how we are doing and how we can improve
                the experience even further.
              </Text>
            </View>

            <TouchableOpacity
              style={{
                backgroundColor: "#4DD1D1",
                height: 45,
                alignItems: "center",
                justifyContent: "center",
                width: width * 0.75,
              }}
              onPress={() => {
                navigation.navigate("Feedback");
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: 16,
                  fontFamily: "Roboto-Bold",
                }}
              >
                Send Feedback
              </Text>
            </TouchableOpacity>
          </View>
        </Card>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    marginTop: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
});

export default About;
