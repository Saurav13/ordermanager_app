import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
  TextInput,
  ActivityIndicator,
  AlertIOS,
  ToastAndroid,
  Platform,
} from "react-native";
import HeaderMenu from "../components/Header";
import { Card } from "react-native-elements";
import { FontAwesome5 } from "@expo/vector-icons";
import { Icon } from "react-native-elements";
import { UserContext } from "../navigation/UserProvider";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const Accept = ({ route, navigation }) => {
  const { detail, setStatus, setTimer } = route.params;
  const [deliveryTime, setDeliveryTime] = useState("00");
  const [dataSending, setDataSending] = useState(false);

  const { baseURL } = useContext(UserContext);

  useEffect(() => {}, [detail, deliveryTime]);

  const { user, venue } = useContext(UserContext);
  const handleDelivery = () => {
    setDataSending(true);
    if (deliveryTime == "00" || deliveryTime == "0") {
      Platform.OS === "android"
        ? ToastAndroid.show(
            "Please Enter/Select Delivery Time.",
            ToastAndroid.LONG
          )
        : AlertIOS.alert("Please Enter/Select Delivery Time.");
      setDataSending(false);
      return;
    }

    user.getIdToken().then(function (idToken) {
      const url = baseURL + `/venues/${venue}/orders/${detail.id}/update`;
      fetch(url, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + idToken,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          status: "Accepted",
          delivery_time: Number(deliveryTime),
        }),
      })
        .then((res) => {
          let orderStatus = JSON.stringify(res).status;
          if (orderStatus == 422) {
            setDeliveryTime("00");
            setDataSending(false);
            Platform.OS === "android"
              ? ToastAndroid.show(
                  "Order Status cannot be updated.",
                  ToastAndroid.LONG
                )
              : AlertIOS.alert("Order Status cannot be updated.");
          } else {
            setStatus("Accepted");
            setTimer(Number(deliveryTime) * 60);
            setDataSending(false);
            setDeliveryTime("00");
            Platform.OS === "android"
              ? ToastAndroid.show("Order has been accepted.", ToastAndroid.LONG)
              : AlertIOS.alert("Order has been accepted.");
            navigation.navigate("Orders");
          }
        })
        .catch((res) => {
          setDeliveryTime("00");
          setDataSending(false);
          Platform.OS === "android"
            ? ToastAndroid.show(
                "Order Status cannot be updated.",
                ToastAndroid.LONG
              )
            : AlertIOS.alert("Order Status cannot be updated.");
        });
    });
  };

  return (
    <ScrollView style={{ backgroundColor: "#e2e2e2" }}>
      <HeaderMenu
        navigation={navigation}
        headerTitle={
          <Text
            style={{
              fontFamily: "Roboto-Bold",
              fontSize: 18,
            }}
          >
            Accept Order #{detail.id}
          </Text>
        }
        rightComponent={<View></View>}
        leftComponent={
          <TouchableOpacity
            onPress={() => {
              setDeliveryTime("00");
              navigation.goBack();
            }}
          >
            <View style={{ paddingLeft: 15 }}>
              <Icon
                name="arrow-left"
                size={22}
                color="#333"
                type="font-awesome"
              />
            </View>
          </TouchableOpacity>
        }
      />
      <Card>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <View style={{ width: "10%", alignItems: "flex-end" }}>
            {detail.order_type === "Delivery" ? (
              <Image
                style={{ height: 30, width: 30 }}
                source={require("../../assets/images/Delivery.png")}
              />
            ) : (
              <Image
                style={{ height: 30, width: 30 }}
                source={require("../../assets/images/Pickup.png")}
              />
            )}
          </View>
          <View
            style={{
              width: "90%",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{
                fontSize: 16,
                paddingTop: 5,
                paddingLeft: 15,
                alignItems: "flex-start",
                fontFamily: "Roboto-Bold",
              }}
            >
              {detail.order_type === "Delivery"
                ? "Delivery Time"
                : "Pickup Time"}
            </Text>
            <Text
              style={{
                fontSize: 16,
                paddingTop: 5,
                alignItems: "flex-end",
                fontFamily: "Roboto-Regular",
              }}
            >
              {deliveryTime + ":00"}
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            marginTop: 25,
            flexDirection: "row",
          }}
        >
          <TouchableOpacity onPress={() => setDeliveryTime("15")}>
            <View
              style={{
                height: height * 0.12,
                width: width * 0.25,
                marginRight: width * 0.04,
                borderRadius: 12,
                backgroundColor: deliveryTime == "15" ? "#25de5b" : "#4DD1D1",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 28,
                  color: "#fff",
                }}
              >
                15
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#fff",
                }}
              >
                minutes
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setDeliveryTime("30")}>
            <View
              style={{
                height: height * 0.12,
                width: width * 0.25,
                marginRight: width * 0.04,
                borderRadius: 12,
                backgroundColor: deliveryTime == "30" ? "#25de5b" : "#4DD1D1",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 28,
                  color: "#fff",
                }}
              >
                30
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#fff",
                }}
              >
                minutes
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setDeliveryTime("45")}>
            <View
              style={{
                height: height * 0.12,
                width: width * 0.25,
                marginRight: width * 0.04,
                borderRadius: 12,
                backgroundColor: deliveryTime == "45" ? "#25de5b" : "#4DD1D1",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 28,
                  color: "#fff",
                }}
              >
                45
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#fff",
                }}
              >
                minutes
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            marginTop: 25,
            flexDirection: "row",
          }}
        >
          <TouchableOpacity onPress={() => setDeliveryTime("60")}>
            <View
              style={{
                height: height * 0.12,
                width: width * 0.25,
                marginRight: width * 0.04,
                borderRadius: 12,
                backgroundColor: deliveryTime == "60" ? "#25de5b" : "#4DD1D1",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 28,
                  color: "#fff",
                }}
              >
                60
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#fff",
                }}
              >
                minutes
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setDeliveryTime("75")}>
            <View
              style={{
                height: height * 0.12,
                width: width * 0.25,
                marginRight: width * 0.04,
                borderRadius: 12,
                backgroundColor: deliveryTime == "75" ? "#25de5b" : "#4DD1D1",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 28,
                  color: "#fff",
                }}
              >
                75
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#fff",
                }}
              >
                minutes
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setDeliveryTime("90")}>
            <View
              style={{
                height: height * 0.12,
                width: width * 0.25,
                marginRight: width * 0.04,
                borderRadius: 12,
                backgroundColor: deliveryTime == "90" ? "#25de5b" : "#4DD1D1",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 28,
                  color: "#fff",
                }}
              >
                90
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: "#fff",
                }}
              >
                minutes
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            marginTop: 45,
          }}
        >
          <View style={{ width: "30%", alignItems: "flex-end" }}>
            <Text
              style={{
                fontFamily: "Roboto-Bold",
                fontSize: 16,
                marginTop: 5,
              }}
            >
              Enter Time
            </Text>
            <Text
              style={{
                alignSelf: "center",
                color: "#888",
                fontFamily: "Roboto-Regular",
              }}
            >
              (minutes)
            </Text>
          </View>
          <View
            style={{
              width: "40%",
              backgroundColor: "#4DD1D1",
              flexDirection: "row",
              justifyContent: "space-between",
              padding: 10,
              marginLeft: 60,
              borderRadius: 20,
            }}
          >
            <TextInput
              placeholder={"60"}
              placeholderTextColor="#fff"
              style={{
                width: "100%",
              }}
              keyboardType={"numeric"}
              onChangeText={(value) => setDeliveryTime(value)}
            />
            <FontAwesome5
              style={{ padding: 6, marginLeft: -25 }}
              color="#fff"
              name="pencil-alt"
              size={18}
            />
          </View>
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 25,
          }}
        >
          <TouchableOpacity
            style={{
              marginTop: 5,
              backgroundColor: "#4DD1D1",
              height: 45,
              alignItems: "center",
              justifyContent: "center",
              width: width * 0.75,
            }}
            onPress={() => {
              handleDelivery();
            }}
          >
            {dataSending ? (
              <ActivityIndicator size="large" color="#fff" />
            ) : (
              <Text
                style={{
                  color: "#fff",
                  fontSize: 16,
                  fontFamily: "Roboto-Bold",
                }}
              >
                ACCEPT & CONFIRM
              </Text>
            )}
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            padding: 10,
          }}
        >
          <Text
            style={{
              fontSize: 13,
              width: width * 0.85,
              fontFamily: "Roboto-Regular",
              color: "#888",
            }}
          >
            We'll send a confirmation including delivery time to your customer.
          </Text>
        </View>
      </Card>
    </ScrollView>
  );
};

const styles = StyleSheet.create({});

export default Accept;
