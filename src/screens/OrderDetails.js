import { FontAwesome5 } from "@expo/vector-icons";
import { ActionSheet, Root } from "native-base";
import React, { useContext, useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  Alert,
  AlertIOS,
  Dimensions,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { Card, Icon } from "react-native-elements";
import MapView from "react-native-maps";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

import HeaderMenu from "../components/Header";
import { UserContext } from "../navigation/UserProvider";
import {
  copyToClipBoard,
  formatTime,
  getIcon,
  getTextColor,
  openMap,
} from "../utils/utils";

const options = ["Pending", "Accepted", "Completed", "Rejected"];

var BUTTONS = [
  { text: "Copy To ClipBoard", icon: "clipboard" },
  { text: "Cancel", icon: "close" },
];
var CANCEL_INDEX = 1;

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const aspectRatio = height / width;

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const OrderDetail = ({ route, navigation }) => {
  const {
    detail,
    status,
    timer,
    setStatus,
    setTimer,
    counterFlag,
  } = route.params;
  const [updateStatusLoading, setUpdateStatusLoading] = useState(false);
  const [parentTimer, setParentTimer] = useState(null);
  const [orderDetail, setOrderDetail] = useState(null);
  const [detailStatus, setDetailStatus] = useState(null);
  const [detailCounterFlag, setDetailCounterFlag] = useState(null);
  const prevCounterFlag = usePrevious(counterFlag);
  const prevTimer = usePrevious(timer);
  const prevDetailID = usePrevious(detail.id);
  const prevStatus = usePrevious(status);

  const { user, venue, baseURL } = useContext(UserContext);

  useEffect(() => {
    if (prevTimer != timer) {
      setParentTimer(timer);
    }
    if (prevCounterFlag != detailCounterFlag) {
      setDetailCounterFlag(counterFlag);
    }
    if (!prevDetailID || prevDetailID != detail.id) {
      setDetailCounterFlag("down");
      setOrderDetail(detail);
    }
    if (!prevStatus || prevStatus != status) {
      setDetailStatus(status);
    }
    if (detailStatus === "Pending") {
      if (parentTimer >= 900) {
        setParentTimer(null);
        setDetailCounterFlag(null);
        setDetailStatus("AutoRejected");
      }
      if (parentTimer > 180) setDetailCounterFlag("up");
      if (parentTimer == 0) setDetailCounterFlag("up");

      if (detailCounterFlag === "down") {
        const countDown = setTimeout(() => {
          setParentTimer(parentTimer - 1);
        }, 1000);
        return () => clearInterval(countDown);
      }

      if (detailCounterFlag === "up") {
        const countUp = setTimeout(() => {
          setParentTimer(parentTimer + 1);
        }, 1000);
        return () => clearInterval(countUp);
      }
    } else if (detailStatus == "Accepted") {
      if (parentTimer <= 0) {
        setParentTimer(null);
        setStatus("AutoRejected");
      } else {
        const countDown = setTimeout(() => {
          setParentTimer(parentTimer - 1);
        }, 1000);
        return () => clearInterval(countDown);
      }
    }
  }, [
    timer,
    parentTimer,
    counterFlag,
    detailStatus,
    status,
    detail,
    detailCounterFlag,
  ]);

  const updateOrder = (updateStatus) => {
    setUpdateStatusLoading(true);
    user.getIdToken().then(function (idToken) {
      let url = baseURL + `/venues/${venue}/orders/${detail.id}/`;
      if (updateStatus === "Refunded") {
        url = url + "refund";
      } else {
        url = url + "update";
      }
      fetch(url, {
        method: "POST",
        headers: {
          Authorization: "Bearer " + idToken,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          status: updateStatus,
        }),
      })
        .then((res) => {
          let orderStatus = JSON.stringify(res).status;
          if (orderStatus == 422) {
            Platform.OS === "android"
              ? ToastAndroid.show(
                  "Order Status cannot be updated.",
                  ToastAndroid.LONG
                )
              : AlertIOS.alert("Order Status cannot be updated.");
            setUpdateStatusLoading(false);
          } else {
            setStatus(updateStatus);
            Platform.OS === "android"
              ? ToastAndroid.show(
                  "Order has been " + updateStatus.toLowerCase() + ".",
                  ToastAndroid.LONG
                )
              : AlertIOS.alert(
                  "Order has been " + updateStatus.toLowerCase() + "."
                );
            setUpdateStatusLoading(false);
            navigation.navigate("Orders");
          }
        })
        .catch((res) => {
          setUpdateStatusLoading(false);
          Platform.OS === "android"
            ? ToastAndroid.show(
                "Order Status cannot be updated.",
                ToastAndroid.LONG
              )
            : AlertIOS.alert("Order Status cannot be updated.");
        });
    });
  };

  return (
    <Root>
      {orderDetail && detailStatus ? (
        <View style={{ backgroundColor: "#e2e2e2", flex: 1 }}>
          <HeaderMenu
            navigation={navigation}
            headerTitle={
              <View style={{ alignItems: "center", marginTop: 1 }}>
                <Text style={{ color: "#000", fontFamily: "Roboto-Bold" }}>
                  {"#" + orderDetail.id}
                </Text>
                <Text style={{ color: "#000", fontFamily: "Roboto-Bold" }}>
                  {orderDetail.order_placed_at}
                </Text>
              </View>
            }
            leftComponent={
              <TouchableOpacity
                onPress={() => {
                  navigation.goBack();
                }}
              >
                <View style={{ paddingLeft: 15 }}>
                  <Icon
                    name="arrow-left"
                    size={22}
                    color="#333"
                    type="font-awesome"
                  />
                </View>
              </TouchableOpacity>
            }
            rightComponent={
              <TouchableOpacity
                style={{ width: 40, paddingRight: 11 }}
                onPress={() =>
                  ActionSheet.show(
                    {
                      options: BUTTONS,
                      cancelButtonIndex: CANCEL_INDEX,
                      title: "",
                    },
                    (args) => {
                      switch (args) {
                        case 0:
                          copyToClipBoard(detail);
                          break;
                        default:
                          break;
                      }
                    }
                  )
                }
              >
                <Icon
                  name="ellipsis-v"
                  size={22}
                  color="#333"
                  type="font-awesome"
                />
              </TouchableOpacity>
            }
          />
          <View
            style={{
              borderTopColor: "#e2e2e2",
              borderTopWidth: 1,
              backgroundColor: "#fff",
              borderBottomColor: "#e2e2e2",
              borderBottomWidth: 1,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <View style={{ ...styles.stickyIconContainer, paddingLeft: 18 }}>
              {getIcon(detailStatus)}
              <Text
                style={{
                  fontFamily: "Roboto-Bold",
                  fontSize: 14,
                  color: getTextColor(detailStatus),
                }}
              >
                {detailStatus === "AutoRejected"
                  ? "Auto Rejected"
                  : detailStatus}
              </Text>
            </View>

            <View style={{ ...styles.stickyIconContainer, paddingRight: 18 }}>
              <View>
                {parentTimer &&
                (status === "Pending" || status === "Accepted") ? (
                  <View style={{ flexDirection: "row" }}>
                    <FontAwesome5
                      name="stopwatch"
                      size={15}
                      style={{ paddingTop: 1 }}
                      color={
                        status === "Pending"
                          ? detailCounterFlag === "up"
                            ? "#d4322e"
                            : "#ffad37"
                          : "#20c281"
                      }
                    />
                    <Text
                      style={{
                        marginLeft: 5,
                        fontFamily: "Roboto-Bold",
                        fontSize: 14,
                        color:
                          status === "Pending"
                            ? detailCounterFlag === "up"
                              ? "#d4322e"
                              : "#ffad37"
                            : "#20c281",
                      }}
                    >
                      {formatTime(parentTimer)}
                    </Text>
                  </View>
                ) : null}
              </View>
            </View>
          </View>
          <ScrollView>
            <Card>
              <View style={styles.OrderDetailContainers}>
                <View style={styles.orderDetailItem}>
                  <View style={styles.orderDetailIconContainer}>
                    <FontAwesome5
                      style={{ color: "#888" }}
                      name="user"
                      size={18}
                    />
                  </View>
                  <View style={styles.orderDetailTextContainer}>
                    <Text style={styles.orderDetailText}>
                      {orderDetail.customer.first_name}{" "}
                      {orderDetail.customer.last_name}
                    </Text>
                  </View>
                </View>

                {orderDetail.customer.dob ? (
                  <View style={styles.orderDetailItem}>
                    <View style={styles.orderDetailIconContainer}>
                      <FontAwesome5
                        style={{ color: "#888" }}
                        name="calendar-alt"
                        size={18}
                      />
                    </View>
                    <View style={styles.orderDetailTextContainer}>
                      <Text style={styles.orderDetailText}>
                        {orderDetail.customer.dob}
                      </Text>
                    </View>
                  </View>
                ) : null}

                <View style={styles.orderDetailItem}>
                  <View style={styles.orderDetailIconContainer}>
                    <FontAwesome5
                      style={{ color: "#888" }}
                      name="phone-square"
                      size={18}
                    />
                  </View>
                  <View style={styles.orderDetailTextContainer}>
                    <Text style={styles.orderDetailText}>
                      {orderDetail.customer.phone_no}
                    </Text>
                  </View>
                </View>

                <View style={styles.orderDetailItem}>
                  <View style={styles.orderDetailIconContainer}>
                    <FontAwesome5
                      style={{ color: "#888" }}
                      name="envelope"
                      size={18}
                    />
                  </View>
                  <View style={styles.orderDetailTextContainer}>
                    <Text style={styles.orderDetailText}>
                      {orderDetail.customer.email}
                    </Text>
                  </View>
                </View>

                <View style={styles.orderDetailItem}>
                  <View style={styles.orderDetailIconContainer}>
                    <FontAwesome5
                      style={{ color: "#888" }}
                      name="dollar-sign"
                      size={18}
                    />
                  </View>
                  <View
                    style={{
                      ...styles.orderDetailTextContainer,
                    }}
                  >
                    <Text
                      style={{
                        ...styles.orderDetailText,
                        backgroundColor: "#4DD1D1",
                        paddingRight: 10,
                        paddingLeft: 10,
                        paddingTop: 3,
                        paddingBottom: 4,
                        borderRadius: 10,
                        color: "#fff",
                      }}
                    >
                      {orderDetail.payment_type=='Over the Phone'?'Pay to delivery driver (NOT PAID)':orderDetail.payment_type}
                    </Text>
                  </View>
                </View>

                {orderDetail.special_request ? (
                  <View style={styles.orderDetailItem}>
                    <View style={styles.orderDetailIconContainer}>
                      <FontAwesome5
                        style={{ color: "#888" }}
                        name="comment-alt"
                        size={18}
                      />
                    </View>
                    <View
                      style={{
                        ...styles.orderDetailTextContainer,
                      }}
                    >
                      <Text
                        style={{
                          ...styles.orderDetailText,
                          backgroundColor: "#4DD1D1",
                          paddingRight: 10,
                          paddingLeft: 10,
                          paddingTop: 3,
                          paddingBottom: 4,
                          borderRadius: 10,
                          color: "#333",
                        }}
                      >
                        {orderDetail.special_request}
                      </Text>
                    </View>
                  </View>
                ) : null}

                {orderDetail.order_type === "Delivery" ? (
                  <View style={styles.orderDetailItem}>
                    <View style={styles.orderDetailIconContainer}>
                      <FontAwesome5
                        style={{ color: "#888" }}
                        name="map-marker"
                        size={18}
                      />
                    </View>
                    <View style={styles.orderDetailTextContainer}>
                      <Text style={styles.orderDetailText}>
                        {orderDetail.customer.street},{" "}
                        {orderDetail.customer.address},{" "}
                        {orderDetail.customer.postcode}
                      </Text>
                    </View>
                  </View>
                ) : null}
              </View>

              {orderDetail.order_type === "Delivery" &&
              orderDetail.customer.location ? (
                <TouchableOpacity
                  delayPressIn={1000}
                  onPress={() =>
                    openMap(
                      Number(orderDetail.customer.location.lat),
                      Number(orderDetail.customer.location.lng)
                    )
                  }
                >
                  <View style={styles.mapContainer}>
                    <MapView
                      style={styles.mapView}
                      region={{
                        latitude: Number(orderDetail.customer.location.lat),
                        longitude: Number(orderDetail.customer.location.lng),
                        latitudeDelta: 0.0042,
                        longitudeDelta:
                          (Dimensions.get("screen").width /
                            Dimensions.get("screen").height) *
                          0.0042,
                      }}
                    >
                      <MapView.Marker
                        coordinate={{
                          latitude: Number(orderDetail.customer.location.lat),
                          longitude: Number(orderDetail.customer.location.lng),
                        }}
                      />
                    </MapView>
                  </View>
                </TouchableOpacity>
              ) : null}

              <View style={styles.ordersContainer}>
                {orderDetail.order_items.map((orderItems, index) => (
                  <View key={index} style={styles.orderListItems}>
                    <View style={styles.orderItemDescription}>
                      <Text style={styles.orderItemHeading}>
                        {orderItems.qty}X {orderItems.name}
                      </Text>
                      {orderItems.variant ? (
                        <View key={index} style={styles.orderItemsAddition}>
                          <Text>
                            {orderItems.variant.name}: (+
                            {parseFloat(orderItems.variant.price).toFixed(2)})
                          </Text>
                        </View>
                      ) : null}
                      {orderItems.options
                        ? orderItems.options.map((extra, index) => (
                            <View key={index} style={styles.orderItemsAddition}>
                              <Text>
                                {extra.group_name}: {extra.name} {extra.qty} (+
                                {parseFloat(extra.price).toFixed(2)})
                              </Text>
                            </View>
                          ))
                        : null}
                      {orderItems.note ? (
                        <View>
                          <Text style={{ marginTop: 0, paddingLeft: 10 }}>
                            {"Note: " + orderItems.note}
                          </Text>
                        </View>
                      ) : null}
                    </View>
                    <View style={styles.orderItemPrice}>
                      <Text style={styles.orderItemPriceText}>
                        {parseFloat(orderItems.total_item_price).toFixed(2)}
                      </Text>
                    </View>
                  </View>
                ))}
              </View>

              <View style={styles.subTotalContainer}>
                <View style={styles.subTotalItems}>
                  <View style={styles.subTotalItemLabel}>
                    <Text style={styles.subTotalItemLabelText}>Sub-total</Text>
                  </View>
                  <View style={styles.subTotalItemPrice}>
                    <Text style={styles.subTotalItemPriceText}>
                      {parseFloat(orderDetail.subtotal).toFixed(2)}
                    </Text>
                  </View>
                </View>

                <View style={styles.subTotalItems}>
                  <View style={styles.subTotalItemLabel}>
                    <Text>Discount</Text>
                  </View>
                  <View style={styles.subTotalItemPrice}>
                    <Text>-{parseFloat(orderDetail.discount).toFixed(2)}</Text>
                  </View>
                </View>
                {orderDetail.order_type === "Delivery" ? (
                  <View style={styles.subTotalItems}>
                    <View style={styles.subTotalItemLabel}>
                      <Text>Delivery Charge</Text>
                    </View>
                    <View style={styles.subTotalItemPrice}>
                      <Text>+{parseFloat(orderDetail.delivery_charge).toFixed(2)}</Text>
                    </View>
                  </View>
                ) : null}

                <View style={styles.subTotalItems}>
                  <View style={styles.subTotalItemLabel}>
                    <Text>Tax</Text>
                  </View>
                  <View style={styles.subTotalItemPrice}>
                    <Text>{parseFloat(orderDetail.tax).toFixed(2)}</Text>
                  </View>
                </View>
              </View>
              <View style={styles.grandTotalContainer}>
                <View style={styles.subTotalItems}>
                  <View style={styles.subTotalItemLabel}>
                    <Text style={styles.subTotalItemLabelText}>Total</Text>
                  </View>
                  <View style={styles.subTotalItemPrice}>
                    <Text style={styles.subTotalItemPriceText}>
                      {parseFloat(orderDetail.grand_total).toFixed(2)}
                    </Text>
                  </View>
                </View>
              </View>
            </Card>

            <View style={{ marginBottom: height / 11 }}></View>
          </ScrollView>

          {detailStatus == "Pending" ? (
            <View style={styles.bottomButtonContainer}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate("Accept", { detail, setStatus, setTimer })
                }
                style={styles.acceptBtn}
              >
                <Text style={styles.acceptBtnText}>ACCEPT</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  Alert.alert(
                    "Are you sure you want to reject the order ?",
                    "",
                    [
                      { text: "NO", onPress: () => null, style: "cancel" },
                      { text: "YES", onPress: () => updateOrder("Rejected") },
                    ]
                  );
                }}
                style={styles.rejectBtn}
              >
                {updateStatusLoading ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  <Text style={styles.rejectBtnText}>REJECT</Text>
                )}
              </TouchableOpacity>
            </View>
          ) : null}
          {detailStatus == "Accepted" ? (
            <View
              style={{
                ...styles.bottomButtonContainer,
                justifyContent: "center",
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  Alert.alert(
                    "Are you sure you want to cancel the order ?",
                    "",
                    [
                      { text: "NO", onPress: () => null, style: "cancel" },
                      { text: "YES", onPress: () => updateOrder("Cancelled") },
                    ]
                  );
                }}
                style={styles.rejectBtn}
              >
                {updateStatusLoading ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  <Text style={styles.rejectBtnText}>CANCEL</Text>
                )}
              </TouchableOpacity>
            </View>
          ) : null}
          {detailStatus == "Cancelled" &&
          orderDetail.payment_type == "Credit" ? (
            <View
              style={{
                ...styles.bottomButtonContainer,
                justifyContent: "center",
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  Alert.alert(
                    "Are you sure you want to refund the order ?",
                    "",
                    [
                      { text: "NO", onPress: () => null, style: "cancel" },
                      { text: "YES", onPress: () => updateOrder("Refunded") },
                    ]
                  );
                }}
                style={styles.rejectBtn}
              >
                {updateStatusLoading ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  <Text style={styles.rejectBtnText}>REFUND</Text>
                )}
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      ) : (
        <View style={styles.logoContainer}>
          <ActivityIndicator size={70} color="#4DD1D1" />
        </View>
      )}
    </Root>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    marginTop: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
  stickyIconContainer: {
    flexDirection: "row",
    paddingTop: 15,
    paddingBottom: 20,
  },
  orderDetailContainers: {
    flex: 1,
    flexWrap: "wrap",
  },
  orderDetailIconContainer: {
    width: "10%",
    paddingTop: 4,
    alignItems: "flex-start",
  },
  orderDetailTextContainer: {
    width: "90%",
    marginLeft: 5,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  orderDetailText: {
    fontFamily: "Roboto-Medium",
    fontSize: 14,
  },
  orderDetailItem: {
    flexDirection: "row",
    padding: 5,
  },
  mapContainer: {
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: hp("34%") + 10,
    marginTop: 30,
  },
  mapView: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    height: hp("34%"),
    width: aspectRatio < 1.6 ? wp("90%") : wp("85%"),
    position: "absolute",
  },
  bottomButtonContainer: {
    backgroundColor: "#dfd7d7",
    height: height / 12,
    bottom: 0,
    position: "absolute",
    width: width,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  acceptBtn: {
    borderColor: "#fff",
    backgroundColor: "#20c281",
    width: 150,
    height: 35,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 20,
  },
  rejectBtn: {
    borderColor: "#fff",
    backgroundColor: "#d4322e",
    width: 150,
    height: 35,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 20,
  },
  acceptBtnText: {
    color: "#fff",
    fontFamily: "Roboto-Bold",
  },
  rejectBtnText: {
    color: "#fff",
    fontFamily: "Roboto-Bold",
  },
  ordersContainer: {
    marginTop: 35,
    borderBottomColor: "#e2e2e2",
    borderBottomWidth: 2,
  },
  orderListItems: {
    flex: 1,
    flexWrap: "wrap",
    flexDirection: "row",
    marginBottom: 15,
  },
  orderItemDescription: {
    width: "80%",
    alignItems: "flex-start",
  },
  orderItemPrice: {
    width: "20%",
    alignItems: "flex-end",
  },
  orderItemHeading: {
    fontFamily: "Roboto-Bold",
  },
  orderItemPriceText: {
    fontFamily: "Roboto-Bold",
  },
  orderItemsAddition: {
    paddingLeft: 10,
  },
  subTotalContainer: {
    flex: 1,
    flexWrap: "wrap",
    borderBottomWidth: 2,
    borderBottomColor: "#e2e2e2",
    paddingTop: 7,
  },
  grandTotalContainer: {
    flex: 1,
    flexWrap: "wrap",
    paddingTop: 7,
  },
  subTotalItems: {
    flexDirection: "row",
    marginBottom: 10,
  },
  subTotalItemLabel: {
    width: "80%",
    alignItems: "flex-start",
  },
  subTotalItemPrice: {
    width: "20%",
    alignItems: "flex-end",
  },
  subTotalItemLabelText: {
    fontFamily: "Roboto-Bold",
  },
  subTotalItemPriceText: {
    fontFamily: "Roboto-Bold",
  },
});

export default OrderDetail;
