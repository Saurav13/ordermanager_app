import React, { useContext, useEffect, useState } from "react";
import {
  ActivityIndicator,
  BackHandler,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from "react-native";
import { BASE_URL } from "react-native-dotenv";
import { Card, Icon } from "react-native-elements";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

import HeaderMenu from "../components/Header";
import { UserContext } from "../navigation/UserProvider";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const aspectRatio = height / width;

const DailyReport = ({ navigation }) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [loader, setLoader] = useState(true);
  const [report, setReport] = useState(true);

  const { user, venue, baseURL } = useContext(UserContext);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    let complete_date = year + "-" + month + "-" + day;
    hideDatePicker();
    user.getIdToken(true).then(function (idToken) {
      setLoader(true);
      let url = `${baseURL}/venues/${venue}/report?date=${complete_date}`;
      getReport(url, idToken);
    });
  };

  const getDate = (time = true) => {
    let today = new Date();
    let month = today.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    let date = today.getDate();
    date = date < 10 ? "0" + date : date;
    let minute = today.getMinutes();
    minute = minute < 10 ? "0" + minute : minute;
    let completeData = today.getFullYear() + "-" + month + "-" + date;
    completeData = time
      ? completeData + " " + today.getHours() + ":" + minute
      : completeData;
    return completeData;
  };

  const getReport = async (url, idToken) => {
    await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + idToken,
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setReport(res);
        setLoader(false);
      })
      .catch((error) => {
        const errorMessage =
          "Oops! Something went wrong. Please try again later.";
        Platform.OS === "android"
          ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
          : AlertIOS.alert(errorMessage);
      });
  };

  const reloadPage = (_) => {
    setLoader(true);
    user.getIdToken(true).then(function (idToken) {
      let url = `${baseURL}/venues/${venue}/report?date=${getDate(false)}`;
      getReport(url, idToken);
    });
  };

  useEffect(() => {
    user.getIdToken(true).then(function (idToken) {
      let url = `${baseURL}/venues/${venue}/report?date=${getDate(false)}`;
      getReport(url, idToken);
    });
  }, []);

  return (
    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={false}
          onRefresh={() => {
            reloadPage();
          }}
        />
      }
      style={{ backgroundColor: "#e2e2e2" }}
    >
      <HeaderMenu
        navigation={navigation}
        headerTitle="DAILY REPORT"
        leftComponent={
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <View style={{ paddingLeft: 15 }}>
              <Icon name="bars" size={26} color="#000" type="font-awesome" />
            </View>
          </TouchableOpacity>
        }
        rightComponent={
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              onPress={showDatePicker}
              style={{
                paddingRight: 15,
              }}
            >
              <Icon
                name="calendar"
                size={24}
                color="#000"
                type="font-awesome"
              />
            </TouchableOpacity>
          </View>
        }
      />
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        style={{ fontSize: 10 }}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        is24Hour={false}
      />
      {loader ? (
        <View style={styles.logoContainer}>
          <ActivityIndicator size={70} color="#4DD1D1" />
        </View>
      ) : (
        <View
          style={{
            jutifyContent: "center",
            alignItems: "center",
          }}
        >
          <Card>
            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: "#888",
              }}
            >
              <Text
                style={{
                  ...styles.venueTitle,
                  fontSize: 18,
                  padding: 10,
                }}
              >
                {report.restaurant_name}
              </Text>
            </View>

            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: "#888",
              }}
            >
              <Text style={{ ...styles.venueTitle, fontSize: 17, padding: 5 }}>
                Order Summary
              </Text>
              <Text
                style={{
                  ...styles.venueTitle,
                  color: "#666",
                  paddingBottom: 5,
                }}
              >
                {report.report_date}
              </Text>
            </View>

            <View>
              <Text
                style={{
                  ...styles.reportTitle,
                  alignSelf: "center",
                  marginTop: 10,
                }}
              >
                Summary by Order Type
              </Text>

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                  paddingLeft: aspectRatio < 1.6 ? 16 : 6,
                  paddingRight: aspectRatio < 1.6 ? 16 : 6,
                  marginTop: 10,
                  borderBottomWidth: 1,
                  borderBottomColor: "#888",
                }}
              >
                <Text
                  style={{
                    ...styles.reportDetailText,
                    width: "50%",
                  }}
                >
                  Type
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    textAlign: "center",
                    width: "10%",
                  }}
                >
                  Total
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    textAlign: "right",
                    width: "35%",
                  }}
                >
                  Value
                </Text>
              </View>

              {report.data.type.map((item, index) => (
                <View
                  key={index}
                  style={{
                    flexDirection: "row",
                    flexWrap: "wrap",
                    paddingLeft: aspectRatio < 1.6 ? 16 : 6,
                    paddingRight: aspectRatio < 1.6 ? 16 : 6,
                    marginTop: 3,
                    marginBottom: 3,
                  }}
                >
                  <Text
                    style={{
                      ...styles.reportDetailText,
                      flexGrow: 1,
                      textAlign: "left",
                      width: "50%",
                      marginBottom: 3,
                      marginTop: 3,
                    }}
                  >
                    {item.name}
                  </Text>
                  <Text
                    style={{
                      ...styles.reportDetailText,
                      flexGrow: 1,
                      textAlign: "center",
                      width: "10%",
                      marginBottom: 3,
                      marginTop: 3,
                    }}
                  >
                    {item.count}
                  </Text>
                  <Text
                    style={{
                      ...styles.reportDetailText,
                      flexGrow: 1,
                      textAlign: "right",
                      width: "35%",
                      marginBottom: 3,
                      marginTop: 3,
                    }}
                  >
                    {item.amount}
                  </Text>
                </View>
              ))}
              <View
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: "#888",
                }}
              />
              <View
                style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "space-evenly",
                  borderBottomWidth: 1,
                  borderBottomColor: "#888",
                }}
              >
                <Text
                  style={{
                    ...styles.reportDetailText,
                    width: "50%",
                    paddingLeft: aspectRatio < 1.6 ? 16 : 6,
                    marginTop: 5,
                    marginBottom: 5,
                  }}
                >
                  Total
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    marginTop: 5,
                    marginBottom: 5,
                    textAlign: "center",
                    width: "10%",
                  }}
                >
                  {report.total.count}
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    paddingRight: aspectRatio < 1.6 ? 16 : 6,
                    textAlign: "right",
                    width: "35%",
                    marginTop: 5,
                    marginBottom: 5,
                  }}
                >
                  {report.total.amount}
                </Text>
              </View>
            </View>

            {/* By Payment  */}

            <View style={{ marginTop: 12 }}>
              <Text
                style={{
                  ...styles.reportTitle,
                  alignSelf: "center",
                  marginBottom: 5,
                }}
              >
                Summary by Payment Type
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  marginTop: 10,
                  paddingLeft: aspectRatio < 1.6 ? 16 : 6,
                  paddingRight: aspectRatio < 1.6 ? 16 : 6,
                  borderBottomWidth: 1,
                  borderBottomColor: "#888",
                }}
              >
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    textAlign: "left",
                    width: "50%",
                  }}
                >
                  Type
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    textAlign: "center",
                    width: "10%",
                  }}
                >
                  Total
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    textAlign: "right",
                    width: "35%",
                  }}
                >
                  Value
                </Text>
              </View>
              {report.data.payments.map((item, index) => (
                <View
                  key={index}
                  style={{
                    flexDirection: "row",
                    flexWrap: "wrap",
                    paddingLeft: aspectRatio < 1.6 ? 16 : 6,
                    paddingRight: aspectRatio < 1.6 ? 16 : 6,
                    marginTop: 3,
                    marginBottom: 3,
                  }}
                >
                  <Text
                    style={{
                      ...styles.reportDetailText,
                      flexGrow: 1,
                      textAlign: "left",
                      width: "50%",
                    }}
                  >
                    {item.name}
                  </Text>
                  <Text
                    style={{
                      ...styles.reportDetailText,
                      flexGrow: 1,
                      textAlign: "center",
                      width: "10%",
                    }}
                  >
                    {item.count}
                  </Text>
                  <Text
                    style={{
                      ...styles.reportDetailText,
                      flexGrow: 1,
                      textAlign: "right",
                      width: "35%",
                    }}
                  >
                    {item.amount}
                  </Text>
                </View>
              ))}
              <View
                style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  paddingLeft: aspectRatio < 1.6 ? 16 : 6,
                  paddingRight: aspectRatio < 1.6 ? 16 : 6,
                  marginTop: 5,
                  marginBottom: 5,
                  borderTopWidth: 1,
                  borderTopColor: "#888",
                  borderBottomWidth: 1,
                  borderBottomColor: "#888",
                }}
              >
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    textAlign: "left",
                    width: "50%",
                    marginTop: 5,
                    marginBottom: 5,
                  }}
                >
                  Total
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    textAlign: "center",
                    width: "10%",
                    marginTop: 5,
                    marginBottom: 5,
                  }}
                >
                  {report.total.count}
                </Text>
                <Text
                  style={{
                    ...styles.reportDetailText,
                    flexGrow: 1,
                    textAlign: "right",
                    width: "35%",
                    marginTop: 5,
                    marginBottom: 5,
                  }}
                >
                  {report.total.amount}
                </Text>
              </View>
            </View>
            <View
              style={{
                marginTop: 40,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ fontFamily: "Roboto-Bold" }}>
                Viewed on: {getDate()}
              </Text>
            </View>
          </Card>
        </View>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  dateText: {
    textAlign: "center",
    fontFamily: "Roboto-Bold",
    fontSize: 16,
  },
  venueTitle: {
    alignSelf: "center",
    fontFamily: "Roboto-Bold",
  },
  reportTitle: {
    fontSize: 16,
    fontFamily: "Roboto-Bold",
  },
  reportDetailText: {
    fontSize: 12,
    fontFamily: "Roboto-Medium",
  },
  logoContainer: {
    marginTop: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
  dashedImage: {
    width: aspectRatio < 1.6 ? wp("89%") : wp("85%"),
    height: aspectRatio < 1.6 ? wp("2%") : wp("1%"),
  },
});

export default DailyReport;
