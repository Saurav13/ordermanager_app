import React, { useState, useEffect, useContext } from "react";
import { UserContext } from "../navigation/UserProvider";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  Platform,
  AlertIOS,
  ToastAndroid,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import HeaderMenu from "../components/Header";
import { Card } from "react-native-elements";
import { WebView } from "react-native-webview";
import { Icon } from "react-native-elements";
import { BASE_URL } from "react-native-dotenv";
import AutoHeightWebView from "react-native-autoheight-webview";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const aspectRatio = height / width;

const Terms = ({ navigation }) => {
  const [loading, setLoading] = useState(true);

  const [terms, setTerms] = useState("");
  const { user, baseURL } = useContext(UserContext);

  const getTerms = async (url, idToken) => {
    await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + idToken,
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setTerms(res.content);
        setLoading(false);
      })
      .catch((error) => {
        const errorMessage =
          "Oops! Something went wrong. Please try again later.";
        Platform.OS === "android"
          ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
          : AlertIOS.alert(errorMessage);
      });
  };

  useEffect(() => {
    user.getIdToken(true).then(function (idToken) {
      let url = `${baseURL}/terms-and-conditions`;
      getTerms(url, idToken);
    });
  }, []);

  return (
    <ScrollView style={{ backgroundColor: "#e2e2e2" }}>
      <HeaderMenu
        navigation={navigation}
        headerTitle="TERMS & CONDITIONS"
        rightComponent={<View></View>}
        leftComponent={
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <View style={{ paddingLeft: 15 }}>
              <Icon name="bars" size={26} color="#000" type="font-awesome" />
            </View>
          </TouchableOpacity>
        }
      />
      {loading ? (
        <View style={styles.logoContainer}>
          <ActivityIndicator size={70} color="#4DD1D1" />
        </View>
      ) : (
        <Card>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                fontSize: 14,
                fontFamily: "Roboto-Bold",
                alignSelf: "center",
              }}
            >
              Terms & Conditions and Licesense Agreement
            </Text>
          </View>
          <View style={{ flex: 1, flexDirection: "row", marginTop: 20 }}>
            {terms ? (
              <AutoHeightWebView
                style={{
                  alignSelf: "center",
                  width: aspectRatio < 1.6 ? wp("88%") : wp("82%"),
                }}
                customScript={``}
                customStyle={`
                                        p{
                                            font-size: 14px !important;
                                            text-align: justify;
                                        }
                                    `}
                files={[
                  {
                    href: "cssfileaddress",
                    type: "text/css",
                    rel: "stylesheet",
                  },
                ]}
                source={{
                  html: `<html><head></head><body><p>${terms}</p></body></html>`,
                }}
                scalesPageToFit={true}
                viewportContent={"width=device-width, user-scalable=no"}
              />
            ) : null}
          </View>
        </Card>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    marginTop: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
});

export default Terms;
