import React, { useContext, useEffect, useState } from "react";
import {
  ActivityIndicator,
  AlertIOS,
  Dimensions,
  Platform,
  StyleSheet,
  ToastAndroid,
  View,
} from "react-native";
import { BASE_URL } from "react-native-dotenv";
import { AsyncStorage } from "react-native";
import { Icon } from "react-native-elements";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";

import Firebase from "../../config/Firebase";
import HeaderMenu from "../components/Header";
import registerForPushNotificationsAsync from "../components/PushNotification";
import VenueItem from "../components/VenueItem";
import { UserContext } from "../navigation/UserProvider";

const height = Dimensions.get("window").height;

export default Venue = () => {
  const [venuesList, setVenueList] = useState([]);
  const [loader, setLoader] = useState(true);
  const { user, baseURL } = useContext(UserContext);
  const PUSH_ENDPOINT = baseURL + "/user/expo/unsubscribe";

  const logout = (_) => {
    AsyncStorage.getItem("PUSH_TOKEN")
      .then((res) => {
        if (res) {
          user.getIdToken().then((idToken) => {
            fetch(PUSH_ENDPOINT, {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + idToken,
              },
              body: JSON.stringify({
                expo_token: res,
              }),
            })
              .then((res) => res.json())
              .then((res) => {
                Firebase.auth().signOut();
              
              })
              .catch((err) => {
                Firebase.auth().signOut();
              });
          });
        } else {
          Firebase.auth().signOut();
        }
      })
      .catch((err) => {
        Firebase.auth().signOut();
      });
  };

  const getVenues = async (url, idToken) => {
    console.log(Firebase .auth().currentUser, url);
    await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + idToken,
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) =>{
          const statusCode = res.status;
          const data = res.json();
          return Promise.all([statusCode, data]);
        }
      )
      .then(([status,res]) => {
    // console.log(Firebase.auth().currentUser, url);
        
          if(status==200){
          setLoader(false);
          const errorMessage = "Could not load venues.";
          if (
            (res.message && res.message === "Unauthenticated.") ||
            res.length === 0
          ) {
            Platform.OS === "android"
              ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
              : AlertIOS.alert(errorMessage);
            return;
          }
          setVenueList(res);
        }
        else{
          alert(res.message);
        }
      })
      .catch((error) => {
        Platform.OS === "android"
          ? ToastAndroid.show(errorMessage, ToastAndroid.LONG)
          : AlertIOS.alert(errorMessage);
        return;
      });
  };

  useEffect(() => {
    user.getIdToken().then((idToken) => {
      AsyncStorage.getItem("PUSH_TOKEN").then((res) => {
        if (!res) {
          registerForPushNotificationsAsync(idToken, baseURL).catch((err) => {
            alert("Couldnot register for push notifications.");
          });
        }
      });
      const url = `${BASE_URL}/venues/list`;
      getVenues(url, idToken);
    });
  }, []);

  return (
    <ScrollView
      style={styles.container}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
    >
      <HeaderMenu
        headerTitle="VENUES"
        rightComponent={
          <TouchableOpacity
            onPress={() => {
              logout();
            }}
            style={{
              paddingRight: 15,
            }}
          >
            <Icon name="sign-out" size={26} color="#000" type="font-awesome" />
          </TouchableOpacity>
        }
      />
      {loader ? (
        <View style={styles.logoContainer}>
          <ActivityIndicator size={70} color="#4DD1D1" />
        </View>
      ) : venuesList ? (
        venuesList.map((item) => (
          <VenueItem key={item.id} item={item} currentURL={baseURL} />
        ))
      ) : null}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#e2e2e2",
  },
  logoContainer: {
    marginTop: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
});
