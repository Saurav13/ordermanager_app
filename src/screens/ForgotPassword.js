import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  KeyboardAvoidingView,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import ForgotPasswordForm from "../components/ForgotPasswordForm";
import { TouchableOpacity } from "react-native-gesture-handler";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

export default ForgotPassword = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <KeyboardAvoidingView behavior="position">
        <View style={styles.logoContainer}>
          <Image source={require("../../assets/images/loginLogo.png")} />
          <Text style={styles.logoText}>TAKE ORDERS FOR</Text>
          <Text style={styles.logoText}>YOUR BUSINESS</Text>
        </View>
        <View style={styles.formContainer}>
          <ForgotPasswordForm navigation={navigation} />
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e2e2e2",
  },
  loadingLogoContainer: {
    marginTop: height / 3,
    alignItems: "center",
    justifyContent: "center",
  },
  loadingLogo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
  logoContainer: {
    marginTop: height / 5,
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 100,
    marginBottom: 10,
  },
  logoText: {
    fontFamily: "Roboto-Bold",
    fontSize: 20,
    color: "#333",
  },
  formContainer: {
    alignItems: "center",
    marginTop: 20,
  },
  socialLoginContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
  },
  socialLoginIconContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
  },
  socialLoginContainerTitle: {
    color: "#fff",
    fontFamily: "Roboto-Bold",
  },
  socialIcon: {
    height: 40,
    width: 40,
    margin: 10,
  },
});
