import firebase from 'firebase';
import{
    API_KEY,
    AUTH_DOMAIN,
    DATABASE_URL,
    PROJECT_ID,
    MESSAGE_SENDER_ID,
    APP_ID
} from 'react-native-dotenv';


const firebaseConfig = {
    apiKey: "AIzaSyCUq4GLGK7XSABr4ud2PYAH3a4YL9R0iZg",
    authDomain: "onecorner-development.firebaseapp.com",
    databaseURL: "https://onecorner-development.firebaseio.com",
    projectId: "onecorner-development",
    messagingSenderId: "672538489178",
    appId: "1:672538489178:web:f6a937d1b0df310bb48363"
  };

const Firebase = firebase.initializeApp(firebaseConfig)

export default Firebase