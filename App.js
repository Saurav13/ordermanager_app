import React, { useState } from "react";
import { AppRegistry, StyleSheet, Text, View } from "react-native";
import { name } from "./app.json";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import UserProvider from "./src/navigation/UserProvider";
import Application from "./src/navigation/Application";

//  Must use redux and eject from expo in future, few code refactoring

AppRegistry.registerComponent(name, () => App);

const fetchFonts = () => {
  return Font.loadAsync({
    "Roboto-Bold": require("./assets/fonts/Roboto-Bold.ttf"),
    "Roboto-Regular": require("./assets/fonts/Roboto-Regular.ttf"),
    "Roboto-Medium": require("./assets/fonts/Roboto-Medium.ttf"),
  });
};

export default function App() {
  const [isFontLoaded, setFontLoaded] = useState(false);
  if (!isFontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
      />
    );
  }
  return (
    <UserProvider>
      <Application />
    </UserProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
